use std::process::Command;

fn run_test(threads: i32, maxcount: i32) {
    let output = Command::new("./cmake-build-debug/project3")
        .arg(threads.to_string())
        .arg(maxcount.to_string())
        .output()
        .expect("Failed to run atomiccounter");

    let output_str = String::from_utf8_lossy(&output.stdout);
    println!("{} threads, {} maxcount:\n{}", threads, maxcount, output_str);
}

fn main() {
    for threads in [1, 2, 8] {
        for maxcount in [1000000, 10000000] {
            run_test(threads, maxcount);
        }
    }
}