/*
 * Joseff Ribble
 * Code based on Zybooks chapter 3.7 and lecture slides
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <string.h>
#include <fcntl.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Error: incorrect number of arguments\nUsage: %s <command>\n", argv[0]);
        return 1;
    }

    const char *name = "SM1"; // Shared memory name
    const int SIZE = 4096;    // Shared memory size

    // Create shared memory segment
    int fd = shm_open(name, O_CREAT | O_RDWR, 0666);
    if (fd == -1) {
        perror("shm failed");
        return 1;
    }

    ftruncate(fd, SIZE); // Configure the size of the shared memory segment
    pid_t pid = fork(); // Fork a child process

    if (pid == 0) { // Child process
        // Memory map the shared memory
        double *ptr = mmap(0, SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        if (ptr == MAP_FAILED) {
            perror("Map failed");
            exit(1);
        }

        // Record start time
        struct timeval start_timeval;
        gettimeofday(&start_timeval, NULL);
        double start_time = start_timeval.tv_sec + (start_timeval.tv_usec * 1e-6);

        *ptr = start_time; // Write start time to shared memory
        close(fd); // Close shared memory file descriptor
        execvp(argv[1], &argv[1]); // Execute command

        // If execvp fails
        perror("Command execution failed");
        exit(1);

    } else if (pid > 0) {  // Parent process
        waitpid(pid, NULL, 0); // Wait for the child process to terminate

        // Record end time
        struct timeval end_timeval;
        gettimeofday(&end_timeval, NULL);
        double end_time = end_timeval.tv_sec + (end_timeval.tv_usec * 1e-6);

        // Memory map the shared memory
        double *ptr = mmap(0, SIZE, PROT_READ, MAP_SHARED, fd, 0);
        if (ptr == MAP_FAILED) {
            perror("Map failed");
            return 1;
        }

        double start_time = *ptr; // Read start time from shared memory
        double elapsed_time = end_time - start_time; // Calculate elapsed time
        printf("Elapsed time: %.5f seconds\n", elapsed_time); // Print elapsed time

        // Close and remove shared memory
        close(fd);
        shm_unlink(name);
    } else {
        // Catch error creating fork
        perror("fork");
        return 1;
    }
    return 0;
}
