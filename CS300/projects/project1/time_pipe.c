/*
 * Joseff Ribble
 * Code based on Zybooks chapter 3.7 and lecture slides
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("Error: incorrect number of arguments\nUsage: %s <command>\n", argv[0]);
        return 1;
    }

    // Create a pipe
    int fd[2];
    if (pipe(fd) == -1) {
        perror("pipe");
        return 1;
    }

    // Fork a child process
    pid_t pid = fork();

    // Child process
    if (pid == 0) {
        close(fd[0]); // Close the unused read end of the pipe in the child

        // Initialize starting time
        struct timeval start_time;
        gettimeofday(&start_time, NULL);

        write(fd[1], &start_time, sizeof(struct timeval)); // Write starting time to pipe
        close(fd[1]); // Close write end of the pipe
        execvp(argv[1], &argv[1]); // Execute the command

        // If execvp fails
        perror("Command execution failed");
        exit(1);
    } else if (pid > 0) { // Parent process
        close(fd[1]); // Close the unused write end of the pipe in the parent
        waitpid(pid, NULL, 0); // Wait for the child process to terminate

        // Read starting time from pipe
        struct timeval start_time;
        read(fd[0], &start_time, sizeof(struct timeval));

        close(fd[0]); // Close read end of the pipe

        // Get the end time in the parent after the child has terminated
        struct timeval end_time;
        gettimeofday(&end_time, NULL);

        // Calculate elapsed time
        double elapsed_time = (end_time.tv_sec - start_time.tv_sec) +
                              (end_time.tv_usec - start_time.tv_usec) * 1e-6;
        printf("Elapsed time: %.5f seconds\n", elapsed_time);

        exit(0);
    } else {
        // Catch error creating fork
        perror("fork");
        return 1;
    }
}
