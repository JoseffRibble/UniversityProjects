use std::process::Command;
use std::str;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let num_iterations = 100;
    let num_points_list = vec![1000, 10000, 100000, 1000000, 10000000];

    println!("{:<15} {:<15} {:<25} {:<25}", "Points", "Iterations", "Avg Pi (One Thread)", "Avg Pi (Two Threads)");

    for &num_points in &num_points_list {
        let mut total_onethread: f64 = 0.0;
        let mut total_twothread: f64 = 0.0;

        for _i in 1..=num_iterations {
            // Run pi_onethread
            let onethread_output = Command::new("./pi_onethread")
                .arg(num_points.to_string())
                .output()?;

            if !onethread_output.status.success() {
                eprintln!("Error running pi_onethread");
                return Err("Error running pi_onethread".into());
            }

            let onethread_stdout = str::from_utf8(&onethread_output.stdout)?;
            let pi_onethread: f64 = onethread_stdout.trim().parse()?;

            // Run pi_twothread
            let twothread_output = Command::new("./pi_twothread")
                .arg(num_points.to_string())
                .output()?;

            if !twothread_output.status.success() {
                eprintln!("Error running pi_twothread");
                return Err("Error running pi_twothread".into());
            }

            let twothread_stdout = str::from_utf8(&twothread_output.stdout)?;
            let pi_twothread: f64 = twothread_stdout.trim().parse()?;

            // Add estimates to totals
            total_onethread += pi_onethread;
            total_twothread += pi_twothread;
        }

        // Calculate averages
        let average_onethread = total_onethread / num_iterations as f64;
        let average_twothread = total_twothread / num_iterations as f64;

        // Print the results in a table format
        println!("{:<15} {:<15} {:<25.10} {:<25.10}", num_points, num_iterations, average_onethread, average_twothread);
    }

    Ok(())
}
