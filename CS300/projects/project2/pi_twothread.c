#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

volatile long int points_in_circle = 0;
volatile long int total_points = 0;

// thread args
typedef struct {
    long int num_points;
    unsigned int seed;
} ThreadArgs;

void* generate_points(void* arg) {
    ThreadArgs* args = (ThreadArgs*)arg;
    long int num_points = args->num_points;
    unsigned int seed = args->seed;

    for (long int i = 0; i < num_points; i++) {
        // same code as pi_onethread.c
        double x = (double)rand_r(&seed) / (double)RAND_MAX;
        double y = (double)rand_r(&seed) / (double)RAND_MAX;

        if (sqrt(x*x + y*y) <= 1.0) {
            points_in_circle++;
        }
        total_points++;
    }
    // This code creates race conditions

    pthread_exit(NULL);
}

int main(int argc, char* argv[]) {
    if (argc != 2) {
        printf("Usage: %s <number_of_points>\n", argv[0]);
        return 1;
    }

    long int num_points = atoi(argv[1]);

    // thread argument structures
    ThreadArgs args1, args2;
    args1.num_points = num_points / 2;
    args1.seed = rand(); // random seed for thread 1
    args2.num_points = num_points / 2;
    args2.seed = rand(); // random seed for thread 2

    pthread_t thread1, thread2; // create 2 threads to run generate_points(). Divide num_points by 2 as each thread only needs to run half of them
    pthread_create(&thread1, NULL, generate_points, &args1);
    pthread_create(&thread2, NULL, generate_points, &args2);

    pthread_join(thread1, NULL); // wait for both threads
    pthread_join(thread2, NULL);

    double pi = 4.0 * (double)points_in_circle / (double)total_points; // calculate pi
    printf("%f\n", pi);

    return 0;
}
