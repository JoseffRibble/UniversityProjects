#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// variables changed by generate_points() thread
volatile long int points_in_circle = 0;
volatile long int total_points = 0;

// thread args
typedef struct {
    long int num_points;
    unsigned int seed;
} ThreadArgs;

void* generate_points(void* arg) {
    ThreadArgs* args = (ThreadArgs*)arg;
    long int num_points = args->num_points;
    unsigned int seed = args->seed;

    for (long int i = 0; i < num_points; i++) {
        // use thread-specific seed for rand_r()
        double x = (double)rand_r(&seed) / (double)RAND_MAX;
        double y = (double)rand_r(&seed) / (double)RAND_MAX;

        // if it falls within the circle, increment counter
        if (sqrt(x*x + y*y) <= 1.0) {
            points_in_circle++;
        }
        total_points++;
    }

    pthread_exit(NULL);
}

int main(int argc, char* argv[]) {
    if (argc != 2) {
        printf("Usage: %s <number_of_points>\n", argv[0]);
        return 1;
    }

    long int num_points = atol(argv[1]); // get points

    // thread argument structure
    ThreadArgs args;
    args.num_points = num_points;
    args.seed = rand();  // Random seed for the thread

    pthread_t thread; // initialize thread
    pthread_create(&thread, NULL, generate_points, (void*)&args); // create thread running generate_point()
    pthread_join(thread, NULL); // wait for thread termination

    // calculate pi
    double pi = 4.0 * (double)points_in_circle / (double)total_points;
    printf("%f\n", pi);

    return 0;
}