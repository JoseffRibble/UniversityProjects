
template<typename Type>
class queue {
private:
    int size;
    int capacity;
    int head;
    Type *arr;
public:
    queue() {
        this->size = 0;
        this->capacity = 10;
        this->head = 0;
        this->arr = new Type[capacity];
    }

    queue(int s) {
        this->size = 0;
        this->capacity = s;
        this->head = 0;
        this->arr = new Type[capacity];
    }

    void enqueue(Type x) {
        arr[size % capacity] = x;
        size++;
    }

    int dequeue() {
        Type out = arr[head];
        arr[head] = NULL;
        if(head == capacity-1) {
            head = 0;
        } else {
            head++;
        }

        size--;
        return out;

    };

    queue(const queue &old) {
        capacity = old.capacity;
        size = old.size;
        head = old.head;
        arr = new Type[capacity];
        for (int i = 0; i < capacity; i++) {
            arr[i] = old.arr[i];
        }
    };

    queue& operator=(const queue& rhs) {
        if (this != &rhs) {
            delete[] arr;
            capacity = rhs.capacity;
            size = rhs.size;
            head = rhs.head;
            arr = new Type[capacity];
            for (int i = 0; i < capacity; i++) {
                arr[i] = rhs.arr[i];
            }
        }
        return *this;
    };

    ~queue() {
        delete[] arr;
    };
};
