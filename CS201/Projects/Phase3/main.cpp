#include <iostream>
#include "BHeap.cpp"

int main() {
    char K[6] = {'a','b','c','d','e','f'};

    BHeap<char> H1(K,6);
    H1.printKey();

    return 0;
}
