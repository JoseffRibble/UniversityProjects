#include <iostream>
using namespace std;
#include "CircularDynamicArray.cpp"

template<typename keytype>
class node {
public:
    keytype key;
    node* left;
    node* right;
    CircularDynamicArray<node*> children;

    explicit node(keytype k) {
        this->key = k;
        this->left = nullptr;
        this->right = nullptr;
        this->children = CircularDynamicArray<node*>();
    }

};

template<typename keytype>
class BHeap {
private:
    node<keytype>* min;

    void consolidate() {
        node<keytype>* sizes[30] = {};

        // start at min
        auto current = min;
        sizes[min->children.length()] = min;
        current = min->right;
        int stufftodo = 3;
        while(stufftodo >= 0) {
            while(true) { // loop
                // if sizes list contains current node type and it's another node
                if (sizes[current->children.length()] && current != sizes[current->children.length()]) {
                    stufftodo = 3;
                    // merge
                    node<keytype> *check = sizes[current->children.length()];
                    if (current->key > check->key) {
                        // current becomes a child of check
                        current->left->right = current->right;  // detach current
                        current->right->left = current->left;   // ^
                        current->right = nullptr;               // ^
                        current->left = nullptr;                // ^
                        // if check has children, connect current to them
                        if (check->children.length() >= 1) {
                            current->left = check->children[check->children.length() - 1];
                            check->children[check->children.length() - 1]->right = current;
                        }
                        check->children.addEnd(current);
                        current = check; // become parent
                    } else {
                        // current becomes the parent of check
                        check->left->right = check->right;  // detach check
                        check->right->left = check->left;   // ^
                        check->right = nullptr;             // ^
                        check->left = nullptr;              // ^
                        if (current->children.length() >= 1) {
                            check->left = current->children[current->children.length() - 1];
                            current->children[current->children.length() - 1]->right = check;
                        }
                        current->children.addEnd(check);
                    }
                    sizes[current->children.length() - 1] = nullptr;
                    if(sizes[current->children.length()] == nullptr) { // if there is an opening for this new node,
                        sizes[current->children.length()] = current;
                        break; // move the value in the list right and break
                    }
                }
                    // if there isn't a value, add it too list
                else {
                    sizes[current->children.length()] = current;
                    break;
                }
            }
            // only if nothing left to merge
            if(current->key < min->key){
                min = current;
            }
            current = current->right;
            stufftodo--;
        }
    }

    void traverser(node<keytype>* current) {
        cout << current->key << " ";
        int i = 0;
        while (i < current->children.length()) {
            traverser(current->children[i]);
            i++;
        }
    }

    void deleteNodes(node<keytype>* node) {
        if (node == nullptr) return;

        // recursively delete children if they exist
        while (node->children.length() > 0) {
            deleteNodes(node->children[0]);
            node->children.delFront();
        }

        delete node;
    }

    node<keytype>* copyTree(node<keytype>* otherNode) {
        if (otherNode == nullptr) {
            return nullptr;
        }

        auto newNode = new node<keytype>(otherNode->key); // copy otherNode

        for (int i = 0; i < otherNode->children.length(); i++) { // recursively copy current node's children
            newNode->children.addEnd(copyTree(otherNode->children[i]));
        }
        for (int i = 0; i < newNode->children.length(); i++) { // link newNode's children
            if(i > 0) { // if there is a left child, link it
                newNode->children[i]->left = newNode->children[i - 1];
            }
            if(i + 1 < newNode->children.length()) { // if there is a right child
                newNode->children[i]->right = newNode->children[i + 1];
            }
        }

        return newNode;
    }

public:
    BHeap() {
        min = nullptr;
    }

    BHeap(keytype k[], int s) {
        min = nullptr;
        for(int i = 0; i < s; i++) {
            insert(k[i]);
        }
        consolidate();
    }

    ~BHeap() {
        deleteNodes(min);
    }

    keytype peekKey() {
        return min->key;
    }

    keytype extractMin() {
        keytype output = min->key;
        node<keytype>* deleteMe = min;
        if (min->children.length() > 0) { // if there are children
            min->left->right = min->children[0];
            min->children[0]->left = min->left;
            min->right->left = min->children[min->children.length() - 1];
            min->children[min->children.length() - 1]->right = min->right;
            min = min->children[0];
        } else { // there aren't children
            min->left->right = min->right;
            min->right->left = min->left;
            min = min->right;
        }
        if (min != deleteMe) delete deleteMe;
        consolidate();
        return output;
    }

    void insert(keytype k) {
        auto current = new node<keytype>(k);
        if (!min) {
            min = current; // if there isn't a min, this is the first node
        } else {
            // insert as new B0 tree left of min
            if(min->left) {
                min->left->right = current; // if there is a left node, make it point to inserted node
                current->left = min->left;
            } else {
                min->right = current; // if there isn't a left node, make the list circular
                current->left = min;
            }
            min->left = current;
            current->right = min;

            if (current->key < min->key) {
                min = current;
            }
        }

    }

    void merge(BHeap<keytype> &H2) {
        // down right
        min->left->right = H2.min;
        // up right
        H2.min->left->right = min;
        node<keytype>* temp;
        // up left
        temp = H2.min->left;
        H2.min->left = min->left;
        // down left
        min->left = temp;

        if(H2.min->key < min->key) {
            min = H2.min;
        }
        H2.min = nullptr;
    }

    void printKey() {
        node<keytype>* current = min;
        // traverse min tree
        cout << "B" << current->children.length() << ":" << endl;
        traverser(current);
        cout << endl;

        // traverse other trees
        current = current->right;
        while(current != min) {
            cout << "B" << current->children.length() << ":" << endl;
            traverser(current);
            cout << endl;
            current = current->right;
        }
    }


    BHeap(const BHeap& other) {
        // copy min tree
        min = copyTree(other.min);

        // copy other trees
        node<keytype>* current = min;
        node<keytype>* otherCurrent = other.min->right;
        while(otherCurrent != other.min) {
            current->right = copyTree(otherCurrent);
            current = current->right;
            otherCurrent = otherCurrent->right;
        }
        current->right = min;
    }

    BHeap& operator=(const BHeap& other) {
        if (this != &other) {
            deleteNodes(min);
            min = copyTree(other.min);

            node<keytype>* current = min;
            node<keytype>* temp = nullptr;
            node<keytype>* otherCurrent = other.min->right;
            while(otherCurrent != other.min) {
                current->right = copyTree(otherCurrent);
                temp = current;
                current = current->right;
                current->left = temp;
                otherCurrent = otherCurrent->right;
            }
            current->right = min;
            min->left = current;
        }
        return *this;
    }
};
