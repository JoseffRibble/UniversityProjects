#include <iostream>
using namespace std;

// back = (back - 1 + capacity) % capacity

template<typename elmtype>
class CircularDynamicArray {
private:
    int size;
    int arrCapacity;
    int head;
    int tail;
    elmtype *arr;

    // function to double arr size if called
    void doubleArr() {
        auto *newArr = new elmtype[arrCapacity * 2]; // create new arr
        for (int i = 0; i < size; i++) {
            newArr[i] = arr[(head + i) % arrCapacity]; // move values into new arr, starting at head
        }
        delete[] arr; // delete old arr
        arr = newArr;
        arrCapacity *= 2;
        head = 0;
        tail = size - 1;
    }

    // function to half arr size if called
    void shrinkArr() {
        auto *newArr = new elmtype[arrCapacity / 2];
        for (int i = 0; i < size; i++) {
            newArr[i] = arr[(head + i) % arrCapacity];
        }
        delete[] arr;
        arr = newArr;
        arrCapacity /= 2;
        head = 0;
        tail = size - 1;
    }

    void Quicksort(elmtype arr[], int left, int right) {
        if (left < right) {
            int pivotIndex = Partition(arr, left, right);
            Quicksort(arr, left, pivotIndex - 1);
            Quicksort(arr, pivotIndex + 1, right);
        }
    }

    int Partition(elmtype arr[], int left, int right) {
        int pivotIndex = left + rand() % (right - left + 1);
        elmtype temp = arr[pivotIndex];
        arr[pivotIndex] = arr[right];
        arr[right] = temp;

        elmtype pivot = arr[right];
        int i = left - 1;

        for (int j = left; j < right; ++j) {
            if (arr[j] <= pivot) {
                ++i;
                temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }

        temp = arr[i + 1];
        arr[i + 1] = arr[right];
        arr[right] = temp;

        return i + 1;
    }

    elmtype QSelector(elmtype arr[], int left, int right, int k) {
        int index = Partition(arr, left, right);

        //return
        if (index - left == k - 1)
            return arr[index];

        if (index - left > k - 1)
            return QSelector(arr, left, index - 1, k);

        return QSelector(arr, index + 1, right, k - index + left - 1);
    }

    //makes sure all values are from 0 to size
    void formatArr(elmtype arr[]) {
        for(int i = 0; i < size; i++) {
            arr[i] = this->operator[](i);
        }
    }

public:
    // Default Constructor. The array should be of capacity 2 and size 0.
    CircularDynamicArray() {
        this->size = 0;
        this->arrCapacity = 2;
        this->head = 0;
        this->tail = 0;
        this->arr = new elmtype[arrCapacity];
    }

    // For this constructor the array should be of capacity and size s.
    CircularDynamicArray(int s) {
        this->size = s;
        this->arrCapacity = s;
        this->head = 0;
        this->tail = s - 1;
        this->arr = new elmtype[arrCapacity];
    }

    // Destructor for the class.
    ~CircularDynamicArray() {
        delete[] arr;
    }

    // Traditional [] operator. Should print a message if i is out of bounds
    // and return a reference to value of type elmtype stored in the class for this purpose.
    elmtype& operator[](int i) {
        if(i >= size || i < 0) { // check if it's between head and tail or out of bounds
            cout << "Value is out of bounds: ";
        }
        return arr[(head + i) % arrCapacity];
    }

    // increases the size of the array by 1 and stores v at the end of the array.
    // Should double the capacity when the new element doesn't fit.
    void addEnd(elmtype v) {
        if (size == arrCapacity) { // if it's full, double
            doubleArr();
        }
        arr[(head + size) % arrCapacity] = v;
        size++;
        tail = (head + size - 1) % arrCapacity; // move tail
    }

    // increases the size of the array by 1 and stores v at the beginning of the array.
    // Should double the capacity when the new element doesn't fit.
    // The new element should be the item returned at index 0.
    void addFront(elmtype v) {
        if (size == arrCapacity) { // if it's full, double and reset
            doubleArr();
        }
        head--; // = (head - 1) % arrCapacity;
        if (head < 0) {
            head = arrCapacity - 1; //loop to back
        }
        arr[head] = v;
        size++;
    }

    // reduces the size of the array by 1 at the end.
    // Should shrink the capacity when only 25% of the array is in use after the delete.
    void delEnd() {
        tail--;
        if (tail < 0) {
            tail = arrCapacity; // move tail to end if it loops
        }
        size--;
        if (size <= arrCapacity / 4) {
            shrinkArr();
        }
    };

    // reduces the size of the array by 1 at the beginning of the array.
    // Should shrink the capacity when only 25% of the array is in use after the delete.
    void delFront() {
        head++;
        if (head >= arrCapacity) {
            head = 0; // loop head
        }
        size--;
        if (size <= arrCapacity / 4) {
            shrinkArr();
        }
    };

    // returns the size of the array.
    int length() {
        return size;
    };

    // returns the capacity of the array.
    int capacity() {
        return arrCapacity;
    };

    // Frees any space currently used and starts over with an array of capacity 2 and size 0.
    void clear() {
        delete[] arr;
        this->size = 0;
        this->arrCapacity = 2;
        this->head = 0;
        this->tail = 0;
        this->arr = new elmtype[arrCapacity];
    };

    // returns the kth smallest element in the array using the quickselect algorithm.
    // This method should choose a pivot element at random.
    elmtype QSelect(int k) {
        elmtype *workingArr = new elmtype[size]; //temp (and jank) solution
        formatArr(workingArr);
        srand(0);
        elmtype i = QSelector(workingArr, 0, size - 1, k);
        delete[] workingArr;
        return i;
    };

    // Sorts the values in the array using a comparison based O(N lg N) algorithm.
    // The sort must be stable. (Merge sort)
    void Sort() {
        elmtype *workingArr = new elmtype[size];
        formatArr(workingArr);
        Quicksort(workingArr, 0, size - 1);
        head = 0;
        tail = size - 1;
        arrCapacity = size;
        delete[] arr;
        arr = workingArr;
    };

    // Performs a linear search of the array looking for the item e.
    // Returns the index of the item if found or -1 otherwise.
    int linearSearch(elmtype e) {
        int i = 0;
        while (i <= size) {
            if (arr[(head + i) % arrCapacity] == e) {
                return i;
            }
            i++;
        }
        return -1;
    };

    // Performs a binary search of the array looking for the item e.
    // Returns the index of the item if found or -1 otherwise.
    // This method assumes that the array is in increasing order, but there is no guarantee that the sort method has been called.
    int binSearch(elmtype e) {
        int left = 0;
        int right = size - 1;

        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (arr[(head + mid) % arrCapacity] == e) {
                return (head + mid) % arrCapacity; // Element found, return index
            } else if (arr[(head + mid) % arrCapacity] < e) {
                left = mid + 1; // Search in the right half
            } else {
                right = mid - 1; // Search in the left half
            }
        }
        return -1;

    };

    CircularDynamicArray(const CircularDynamicArray& other)
        : size(other.size), arrCapacity(other.arrCapacity), head(other.head), tail(other.tail) {
        arr = new elmtype[arrCapacity];
        for (int i = 0; i < size; ++i) {
            arr[i] = other.arr[(other.head + i) % other.arrCapacity];
        }
    }

    CircularDynamicArray& operator=(const CircularDynamicArray& other) {
        if (this != &other) {
            delete[] arr;
            size = other.size;
            arrCapacity = other.arrCapacity;
            head = 0;
            tail = other.tail;
            arr = new elmtype[arrCapacity];
            for (int i = 0; i < size; ++i) {
                arr[i] = other.arr[(other.head + i) % other.arrCapacity];
            }
        }
        return *this;
    }



};