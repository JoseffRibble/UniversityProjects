package edu.ua.team7.chocAn;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

public class VerifyProviderTest {
    private StorageServer storageServer;
    private VerifyProvider verifyProvider;
    
    @BeforeEach
    public void setUp() {
        storageServer = new StorageServer();
        verifyProvider = new VerifyProvider(storageServer);
    }

    // Test a valid provider code. 
    @Test
    public void testValidProvider() {
        assertTrue(verifyProvider.verifyProvider("185169123"));
    }

    // Test an invalid provider code (code is null)
    @Test
    public void testNullProvider() {
        assertFalse(verifyProvider.verifyProvider(null));
    }

    // Test an invalid provider code (code is empty)
    @Test 
    public void testEmptyCode() {
        assertFalse(verifyProvider.verifyProvider(""));
    }

    // Test an invalid provider code (code is too short)
    @Test
    public void testShortCode() {
        assertFalse(verifyProvider.verifyProvider("18516912"));
    }
    // Test an invalid provider code (code contains non-digits)
    @Test
    public void testInvalidCode() {
        assertFalse(verifyProvider.verifyProvider("1234a6789"));
    }

}