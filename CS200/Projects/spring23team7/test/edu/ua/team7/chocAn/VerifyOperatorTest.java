package edu.ua.team7.chocAn;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Dylan Brearley
 */

public class VerifyOperatorTest {
    private StorageServer storageServer;
    private VerifyOperator verifyOperator;
    
    @BeforeEach
    public void setUp() {
        storageServer = new StorageServer();
        verifyOperator = new VerifyOperator(storageServer);
    }

    @Test
    public void testValidOperator() {
        assertTrue(verifyOperator.verifyOperator("427651938"));
    }

    @Test
    public void testInvalidOperator() {
        assertFalse(verifyOperator.verifyOperator("621813558"));
    }

    @Test 
    public void wrongCodeSizeOperator() {
        assertFalse(verifyOperator.verifyOperator("123"));
    }

    @Test
    public void noEntryOperator() {
        assertFalse(verifyOperator.verifyOperator(""));
    }

    @Test
    public void testInvalidCode() {
        assertFalse(verifyOperator.verifyOperator("6218B3558"));
    }

}
