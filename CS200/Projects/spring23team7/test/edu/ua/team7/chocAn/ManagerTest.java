package edu.ua.team7.chocAn;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// Avery Fernandez

public class ManagerTest {
    private StorageServer storageServer;
    private Manager manager;

    @BeforeEach
    public void setUp() {
        storageServer = new StorageServer();
        manager = new Manager("John Doe", "12345", storageServer);
    }

    @Test
    public void testGetName() {
        assertEquals("John Doe", manager.getName());
    }

    @Test
    public void testSetName() {
        manager.setName("Jane Doe");
        assertEquals("Jane Doe", manager.getName());
    }

    @Test
    public void testGetId() {
        assertEquals("12345", manager.getId());
    }

    @Test
    public void testSetId() {
        manager.setId("54321");
        assertEquals("54321", manager.getId());
    }

    @Test
    public void testAddMember() {
        assertTrue(manager.addMember("11111", "Alice", "123 Main St", "Anytown", "CA", "12345"));
    }

    @Test
    public void testUpdateMember() {
        manager.addMember("11111", "Alice", "123 Main St", "Anytown", "CA", "12345");
        assertTrue(manager.updateMember("22222", "Alice Updated", "456 Main St", "Anytown", "CA", "12345"));
    }

    @Test
    public void testRemoveMember() {
        manager.addMember("11111", "Alice", "123 Main St", "Anytown", "CA", "12345");
        assertTrue(manager.removeMember("11111"));
    }

    @Test
    public void testAddProvider() {
        assertTrue(manager.addProvider("11111", "Provider 1", "123 Main St", "Anytown", "CA", "12345"));
    }

    @Test
    public void testUpdateProvider() {
        manager.addProvider("11111", "Provider 1", "123 Main St", "Anytown", "CA", "12345");
        assertTrue(manager.updateProvider("22222", "Provider Updated", "456 Main St", "Anytown", "CA", "12345"));
    }

    @Test
    public void testRemoveProvider() {
        manager.addProvider("11111", "Provider 1", "123 Main St", "Anytown", "CA", "12345");
        assertTrue(manager.removeProvider("11111"));
    }

    // Note: The following three tests check if the methods are running without exceptions.

    @Test
    public void testGenerateProviderReport() {
        assertDoesNotThrow(() -> manager.generateProviderReport("Provider 1"));
    }

    @Test
    public void testGenerateManagerReport() {
        assertDoesNotThrow(() -> manager.generateManagerReport());
    }

    @Test
    public void testGenerateMemberReport() {
        assertDoesNotThrow(() -> manager.generateMemberReport("11111"));
    }
}
