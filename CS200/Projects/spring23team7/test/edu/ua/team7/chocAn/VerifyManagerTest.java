package edu.ua.team7.chocAn;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class VerifyManagerTest {
    private StorageServer storageServer;
    private VerifyManager verifyManager;

    @BeforeEach
    public void setUp() {
        storageServer = new StorageServer();
        verifyManager = new VerifyManager(storageServer);

        // Adding a manager to the storageServer
    }

    @Test
    // Checking if 817463529 is verified as a manager
    public void testVerifyManagerValidCode() {
        assertTrue(verifyManager.verifyManager("817463529"));
    }

    @Test
    // Checking if 987654321 is verified as a manager
    public void testVerifyManagerInvalidCode() {
        assertFalse(verifyManager.verifyManager("987654321"));
    }

    @Test
    // Checking if null is verified as a manager
    public void testVerifyManagerNullCode() {
        assertFalse(verifyManager.verifyManager(null));
    }

    @Test
    // Checking if empty string is verified as a manager
    public void testVerifyManagerEmptyCode() {
        assertFalse(verifyManager.verifyManager(""));
    }
}
