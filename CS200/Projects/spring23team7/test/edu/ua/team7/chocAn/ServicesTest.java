package edu.ua.team7.chocAn;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;

/**
 * @author Joseff Ribble
 */
public class ServicesTest {
    private Member member;
    private Services service;

    @BeforeEach
    public void setUp() {
        try {
            member = new Member(397584621);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        service = new Services(member, "2022-01-01", "Test Provider Name", "Test Service Name");
    }

    @Test
    public void testGetMemberName() {
        Assertions.assertEquals("Amanda Davis", service.getMemberName());
    }

    @Test
    public void testGetServiceDate() {
        Assertions.assertEquals("2022-01-01", service.getServiceDate());
    }

    @Test
    public void testGetProviderName() {
        Assertions.assertEquals("Test Provider Name", service.getProviderName());
    }

    @Test
    public void testGenerateManagerReport() {
        ManagerReport managerReport = new ManagerReport();
        Assertions.assertEquals("[158462598,John Doe,123 Main St,Anytown,CA,12345,04-27-2023,Bob Johnson,Sleep consultation]", managerReport.getAllServices().toString());
    }
}
