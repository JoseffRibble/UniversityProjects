package edu.ua.team7.chocAn;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Sonam Lama
 */

public class ManageProviderTest {
    @TempDir
    Path tempDir;
    private StorageServer storageServer;
    private ManageProvider manageProvider;

    @BeforeEach
    public void setUp() {
        storageServer = new StorageServer();
        manageProvider = new ManageProvider(storageServer);
    }

    @Test
    public void testAddProvider() {
        boolean result = manageProvider.addProvider("1299", "Chun Lee", "128 Reed St", "Tuscaloosa", "AL", "35401");
        assertTrue(result, "Adding provider should be successful");

    }

    @Test
    public void testUpdateMember() {
        manageProvider.addProvider("1299", "Chun Lee", "128 Reed St", "Tuscaloosa", "AL", "35401");
        boolean result = manageProvider.updateProvider("1299", "Rifa Lee", "128 Main St", "Greenville", "AL", "35678");
        assertTrue(result, "Updating provider should be successful");

    }

    @Test
    public void testRemoveMember() {
        manageProvider.addProvider("1299", "Chun Lee", "128 Reed St", "Tuscaloosa", "AL", "35401");
        boolean result = manageProvider.removeProvider("1299");
        assertTrue(result, "Removing member should be successful");

        // Additional checks can be added here to verify the contents of the files
    }
}
