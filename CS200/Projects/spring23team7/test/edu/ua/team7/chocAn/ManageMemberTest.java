package edu.ua.team7.chocAn;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;
// Avery Fernandez
import static org.junit.jupiter.api.Assertions.*;

public class ManageMemberTest {

    @TempDir
    Path tempDir;
    private StorageServer storageServer;
    private ManageMember manageMember;

    @BeforeEach
    public void setUp() {
        storageServer = new StorageServer();
        manageMember = new ManageMember(storageServer);
    }

    @Test
    public void testAddMember() {
        boolean result = manageMember.addMember("123", "John Doe", "123 Main St", "Anytown", "AS", "12345");
        assertTrue(result, "Adding member should be successful");

    }

    @Test
    public void testUpdateMember() {
        manageMember.addMember("123", "John Doe", "123 Main St", "Anytown", "AS", "12345");
        boolean result = manageMember.updateMember("123", "Jane Doe", "456 Main St", "Newcity", "BS", "67890");
        assertTrue(result, "Updating member should be successful");

    }

//    @Test
//    public void testRemoveMember() {
//        manageMember.addMember("123", "John Doe", "123 Main St", "Anytown", "AS", "12345");
//        boolean result = manageMember.removeMember("123");
//        assertTrue(result, "Removing member should be successful");
//
//    }
}
