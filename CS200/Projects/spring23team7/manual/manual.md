# Chocoholics Anonymous User Manual

ChocAn is a health care provider management system that allows Operators, Providers, and Managers to perform their respective tasks. This user manual will guide you through setting up the environment, running the program, and testing each feature. By following this manual, you should be able to execute the program and interact with the different functionalities provided by the ChocAn system.

## System Requirements

Before running the program, ensure you have the following installed on your machine:

* Git (version control system)
* Java Development Kit (JDK) 18 or higher

## Cloning the Repository

1. Visit the Bitbucket website and log in with your credentials.
2. Navigate to the repository and select the **Clone** button in the top right-hand corner.
3. You will have the option to automatically clone the repo in **VSCode** or use the provided link to clone in **Eclipse**.

### Cloning in terminal

1. Open a terminal or command prompt.
2. Navigate to the directory where you want to clone the ChocAn repository.
3. Run the following command to clone the repository: 

```shell
git clone https://amfernandez7@bitbucket.org/azaman2/spring23team7.git
```

4. Change to the **spring23team7** directory by running the command: 

```shell
cd spring23team7
```

### Cloning in Eclipse

1. Open the project in Eclipse.
2. Select **File** > **Import** > **Projects from Git**.
3. Paste the link of the Spring23Team7 repository.
4. Choose the destination for the repository and click **Finish**.

## Running the Software in VSCode

1. Press the icon in the upper right-hand corner of the IDE (assuming you're using VSCode) that looks like a **Play** button.
2. The initial terminal will appear in the command line.
3. Choose your role in the system: **Manager**, **Operator**, or **Provider**. The member does not need their own option as all their actions are controlled via the provider.
4. The next set of options will be determined based on your initial selection. Navigate through the various options within the command line to test each functionality of the different actors within the program.

## Running the Software (else)

1. Ensure you are in the **spring23team7** directory.
2. Compile the code using **`ant`**.
3. Move to the release folder:
```
cd release
```
4. Run the java file:
```
java -jar Team7.jar
```

## Running JUnit Tests

1. Click the testing symbol in the left-hand sidebar that resembles an Erlenmeyer flask.
2. You will see a variety of JUnit tests, written by each team member.
3. To run any JUnit test, click the **Play** icon next to the test you want to run.
4. If the test is run successfully, a green checkmark will appear next to the test name, signifying that all functionality ran as intended.

The JUnit tests are able to test the functionality of the **`ManageMember`**, **`ManageProvider`**, **`Services`**, **`Manager`**, **`VerifyManager`**, **`VerifyOperator`**, and **`VerifyProvider`** classes.

## Persistence

The system provides persistence by updating the respective **`.csv`** files during each instance that implicates a change to the data in the Chocoholics Anonymous system:

* **`members.csv`**: Contains a list of each member along with their personal information.
* **`providers.csv`**: Contains a list of each provider along with their information.
* **`provider_directory.csv`**: Contains a list of all services along with their respective codes.

To generate JavaDocs, right-click on the desired file and navigate to the menu option which says **Generate Javadoc**.

## Terminal System Overview

The Chocoholics Anonymous terminal system allows the storage, retrieval, and usage of data related to members, providers, operators, managers, and services.

1. On startup, the terminal will prompt the user to choose their role (1: Operator, 2: Provider, 3: Manager).
2. After successful verification, the next steps will vary based on the user's choice:
    * **Operator Terminal**: Manage providers and members, allowing the operator to add, update, or delete from the ChocAn storage files.
    * **Provider Terminal**: Bill Chocoholics Anonymous, utilize the provider directory, and lookup services.
    * **Manager Terminal**: Request a member, provider, or manager report.
3. After completing any action, the user will return to their respective terminal and have the option to select another action or exit at any time by entering "4" into the terminal.

## Task Distribution

* Avery Fernandez - [Majority of java files, formatting of repo structure, JUnit Test, formatting of manual] - 30%
* Dylan Brearley - [JUnit Test, User Manual, various methods on the files] - 15%
* Gabrielle Karabas - [JUnit Test, User Manual, various methods on the files] - 20%
* Sonam Lama - [Operator Class and Java Docs] - 10%
* Matthew Stewart - [Verify Operator Class, Verify Manager Class, Javadocs] - 10%
* Joseff Ribble - [Ant File, Repo Formatting, Junit Tests, JavaDocs] - 15%
