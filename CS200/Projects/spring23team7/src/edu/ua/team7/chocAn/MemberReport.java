package edu.ua.team7.chocAn;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Generates list of services for member and can write them to file
 */
public class MemberReport {

    private static final String PROVIDED_SERVICES_FILE = "provided_services.csv";

    /**
     * gets list of service strings from storage then writes them to the member file.
     *
     * @param memberId
     * @return list of service strings
     */
    public List<String> getServicesForMember(int memberId) {
        List<String> memberServices = new ArrayList<>();
        String OUTPUT_FILE = "memberReport" + memberId + ".txt";

        try (BufferedReader br = new BufferedReader(new FileReader(PROVIDED_SERVICES_FILE))) {
            String line;
            boolean header = true;
            while ((line = br.readLine()) != null) {
                if (header) {
                    header = false;
                    continue;
                }
                String[] data = line.split(",");
                if (Integer.parseInt(data[0].trim()) == memberId) {
                    memberServices.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        writeServicesToFile(memberServices, OUTPUT_FILE);
        return memberServices;
    }

    /**
     * write list of services to OUTPUT_FILE
     *
     * @param services
     * @param OUTPUT_FILE
     */
    public void writeServicesToFile(List<String> services, String OUTPUT_FILE) {
        try (PrintWriter writer = new PrintWriter(OUTPUT_FILE)) {
            for (String service : services) {
                writer.println(service);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * main runner for MemberReport
     *
     * @param args
     */
    public static void main(String[] args) {
        MemberReport memberReport = new MemberReport();
        List<String> services = memberReport.getServicesForMember(158462598);
        for (String service : services) {
            System.out.println(service);
        }
    }
}
