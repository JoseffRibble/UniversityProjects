package edu.ua.team7.chocAn;

import java.io.FileNotFoundException;
import java.io.File;
import java.util.Scanner;

/**
 * Provider Class
 */
public class Provider {
    private String name;
    private String id;
    private String licenseNumber;
    private ManageMember manageMember;

    /**
     * Provider constructor. Looks in providers.csv for list of valid providers
     *
     * @param id
     * @param storageServer
     * @throws FileNotFoundException
     */
    public Provider(String id, StorageServer storageServer) throws FileNotFoundException {
        File file = new File("providers.csv");
        Scanner scanner = new Scanner(file);
        scanner.nextLine(); // skip the header row
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] fields = line.split(", ");
            if (fields[0].equals(id)) {
                this.name = fields[1];
                this.id = id;
                this.licenseNumber = fields[0]; // Assuming the license number is the same as the ID
                break;
            }
        }
        scanner.close();
        this.manageMember = new ManageMember(storageServer);
    }

    /**
     * Provider constructor which doesn't look in storage
     *
     * @param name
     * @param id
     * @param licenseNumber
     * @param storageServer
     */
    public Provider(String name, String id, String licenseNumber, StorageServer storageServer) {
        this.name = name;
        this.id = id;
        this.licenseNumber = licenseNumber;
        this.manageMember = new ManageMember(storageServer);
    }

    /**
     * get the name of provider
     *
     * @return provider's name
     */
    public String getName() {
        return name;
    }

    /**
     * sets the name of provider
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * get the id of the provider
     *
     * @return provider's id
     */
    public String getId() {
        return id;
    }

    /**
     * set the id of the provider
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * gets license number of Provider
     *
     * @return licenseNumber
     */
    public String getLicenseNumber() {
        return licenseNumber;
    }

    /**
     * set license number of Provider
     *
     * @param licenseNumber
     */
    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    /**
     * get the ManageMember class associated with provider
     *
     * @return manageMember
     */
    public ManageMember getManageMember() {
        return this.manageMember;
    }
}
