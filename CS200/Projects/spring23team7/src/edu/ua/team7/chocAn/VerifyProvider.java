package edu.ua.team7.chocAn;

/**
 * @author Avery
 *
 * VerifyProvider class verifies the identity of manager by looking for their 9 digit code in storage.txt
 */
public class VerifyProvider {

    private StorageServer storageServer;

    /**
     * Default Constructor
     * @param storageServer storage server is an object that deals with the information in storage.txt
     */
    public VerifyProvider(StorageServer storageServer) {
        this.storageServer = storageServer;
    }

     /**
     * verifyManager method looks the 9-digit code in storage.txt
     * @param code 
     * @return returns true if the 9-digit code is found, otherwise returns false
     */
    public boolean verifyProvider(String code) {
        if (code == null || code.isEmpty()) {
            return false;
        }

        Object provider = storageServer.getData("Provider", code);
        if (provider == null) {
            return false;
        }

        return isValidLicense(code);
    }

    /**
     * isValidLicense checks whether the code is valid or not, using the attributes of code like length and char type
     * @param licenseNumber
     * @return  returns true if the 9-digit licenseNumber is valid, otherwise returns false
     */
    private boolean isValidLicense(String licenseNumber) {
        // Implement validation logic for the license number
        if (licenseNumber.length() != 9) {
            return false;
        }

        for (int i = 0; i < licenseNumber.length(); i++) {
            char c = licenseNumber.charAt(i);
            if (!Character.isDigit(c)) {
                return false;
            }
        }

        return true;
    }

}
