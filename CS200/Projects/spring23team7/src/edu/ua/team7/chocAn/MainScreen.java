package edu.ua.team7.chocAn;

import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * The MainScreen class. This is an interface class that directly interacts with user.
 * It integrates all classes and makes them user-functional.
 * @author Avery
 * 
 */
public class MainScreen {
    /**
     * storageServer variable that helps build storage.txt
     */
    private StorageServer storageServer;
    Scanner scanner;

   /**
    * Default Constructor
    */
    public MainScreen() {
        storageServer = new StorageServer();
        scanner = new Scanner(System.in);
        WeeklySchedule.main(null);
        showMainMenu();
    }

    /**
     * showMainmenu method that allows user to interact with the system and choose the user type option
     * @throws NoSuchElementException
     * according to user's choice (input), triggers showLoginScreen Method
     * if invalid argument is provided by the user, a message is printed and a rcursive call is made
     */
    void showMainMenu() {
        System.out.println("\nChocoholics Anonymous");
        System.out.println("1. Operator");
        System.out.println("2. Provider");
        System.out.println("3. Manager");
        System.out.println("4. Exit");
        System.out.print("Enter your choice: ");
        String input = "";
        input = scanner.nextLine();

        switch (input) {
            case "1":
                showLoginScreen("Operator");
                break;
            case "2":
                showLoginScreen("Provider");
                break;
            case "3":
                showLoginScreen("Manager");
                break;
            case "4":
                System.out.println("\nExiting...");
                System.exit(0);
            default:
                System.out.println("Invalid choice. Please enter 1, 2, 3, or 4.");
                showMainMenu(); // Prompt again for valid input
        }
    }

    /**
     * 
     * @param userType the type of user provide by the  user to the system
     * According to the type of user, verification is done with help of classes like VerifyOperator, VerifyProvider, VerifyManager, and VerifyMember
     * if not verified, a message is printed and while loop continues
     * 
     */

    void showLoginScreen(String userType) {
        boolean verified = false;
        String code = "";
        while (!verified) {
            System.out.print("\n" + userType + " Login\n9-digit Code: ");
            code = scanner.nextLine();

            switch (userType) {
                case "Operator":
                    VerifyOperator verifyOperator = new VerifyOperator(storageServer);
                    verified = verifyOperator.verifyOperator(code);
                    break;
                case "Provider":
                    VerifyProvider verifyProvider = new VerifyProvider(storageServer);
                    verified = verifyProvider.verifyProvider(code);
                    break;
                case "Manager":
                    VerifyManager verifyManager = new VerifyManager(storageServer);
                    verified = verifyManager.verifyManager(code);
                    break;
            }
            /**
             * if verified, a message is printed.
             * according to the userType, different methods are triggered
             */
            if (verified) {
                System.out.println(userType + " successfully logged in.");
    
                switch (userType) {
                    case "Operator":
                        operatorScreen(code);
                        break;
                    case "Provider":
                        providerScreen(code);
                        break;
                    case "Manager":
                        managerScreen(code);
                        break;
                }
            } else {
                System.out.println("Invalid code. Please try again.");
            }
        }
    }

    /**
     * operatorScreen class is an interface class for the operator that helps operator to interact with the system
     * @param code 9-digit long code provided to the specied operator
     * A welcome is message is printed and operator is asked to choose their task
     * According to the task choice, operator adds, removes, or updates member or provider
     */

    void operatorScreen(String code) {
        Operator myInfo = null;
        myInfo = new Operator(code, storageServer);
        int choice;
        do {
            System.out.println("Welcome, Operator " + myInfo.getId() + "!");
            System.out.println("1. Manage Providers");
            System.out.println("2. Manage Members");
            System.out.println("3. Exit");
            System.out.print("Enter your choice: ");
            choice = scanner.nextInt();
            scanner.nextLine(); // Consume the newline character left by nextInt()

            /**
             * switch case for member or provider
             */
    
            switch (choice) {
                case 1:
                    int secondChoice;
                    ManageProvider operatorManging = myInfo.getManageProvider();
                    do {
                        System.out.println("Managing Providers!");
                        System.out.println("1. Add Provider");
                        System.out.println("2. Update Providers");
                        System.out.println("3. Remove Providers");
                        System.out.println("4. Go Back");
                        System.out.print("Enter your choice: ");
                        secondChoice = scanner.nextInt();
                        scanner.nextLine(); // Consume the newline character left by nextInt()
                        
                        /**
                        * switch case for add, remove or update provider
                        * 
                        */

                        switch (secondChoice) {
                            case 1:
                                System.out.print("Enter provider number: ");
                                String number = scanner.nextLine();
                                System.out.print("Enter provider name: ");
                                String name = scanner.nextLine();
                                System.out.print("Enter provider address: ");
                                String address = scanner.nextLine();
                                System.out.print("Enter provider city: ");
                                String city = scanner.nextLine();
                                System.out.print("Enter provider state: ");
                                String state = scanner.nextLine();
                                System.out.print("Enter provider zip: ");
                                String zip = scanner.nextLine();
                                operatorManging.addProvider(number, name, address, city, state, zip);
                                break;
                            case 2:
                                System.out.print("Enter provider number: ");
                                number = scanner.nextLine();
                                System.out.print("Enter provider name: ");
                                name = scanner.nextLine();
                                System.out.print("Enter provider address: ");
                                address = scanner.nextLine();
                                System.out.print("Enter provider city: ");
                                city = scanner.nextLine();
                                System.out.print("Enter provider state: ");
                                state = scanner.nextLine();
                                System.out.print("Enter provider zip: ");
                                zip = scanner.nextLine();
                                operatorManging.updateProvider(number, name, address, city, state, zip);
                                break;
                            case 3:
                                System.out.print("Enter provider number: ");
                                number = scanner.nextLine();
                                operatorManging.removeProvider(number);
                                break;
                            case 4:
                                System.out.println("Going Back...");
                                break;
                            default:
                                System.out.println("Invalid choice. Please try again.");
                        }
                    } while (secondChoice != 4);
                    break;

                       /**
                        * switch case for add, remove or update member
                        * 
                        */
                case 2:
                    int nextChoice;
                    ManageMember memberManging = myInfo.getManageMember();
                    do {
                        System.out.println("Managing Members!");
                        System.out.println("1. Add Members");
                        System.out.println("2. Update Members");
                        System.out.println("3. Remove Members");
                        System.out.println("4. Go Back");
                        System.out.print("Enter your choice: ");
                        nextChoice = scanner.nextInt();
                        scanner.nextLine(); // Consume the newline character left by nextInt()
                        switch (nextChoice) {
                            case 1:
                                System.out.print("Enter member number: ");
                                String number = scanner.nextLine();
                                System.out.print("Enter member name: ");
                                String name = scanner.nextLine();
                                System.out.print("Enter member address: ");
                                String address = scanner.nextLine();
                                System.out.print("Enter member city: ");
                                String city = scanner.nextLine();
                                System.out.print("Enter member state: ");
                                String state = scanner.nextLine();
                                System.out.print("Enter member zip: ");
                                String zip = scanner.nextLine();
                                memberManging.addMember(number, name, address, city, state, zip);
                                break;
                            case 2:
                                System.out.print("Enter member number: ");
                                number = scanner.nextLine();
                                System.out.print("Enter member name: ");
                                name = scanner.nextLine();
                                System.out.print("Enter member address: ");
                                address = scanner.nextLine();
                                System.out.print("Enter member city: ");
                                city = scanner.nextLine();
                                System.out.print("Enter member state: ");
                                state = scanner.nextLine();
                                System.out.print("Enter member zip: ");
                                zip = scanner.nextLine();
                                memberManging.updateMember(number, name, address, city, state, zip);
                                break;
                            case 3:
                                System.out.print("Enter member number: ");
                                number = scanner.nextLine();
                                memberManging.removeMember(number);
                                break;
                            case 4:
                                System.out.println("Going Back...");
                                break;
                            default:
                                System.out.println("Invalid choice. Please try again.");
                        }
                    } while (nextChoice != 4);
                    break;
                case 3:
                    System.out.println("Exiting...");
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        } while (choice != 3);
        
    }

     /**
     * providerScreen class is an interface class for the provider that helps the provider to interact with the system
     * @param code 9-digit long code provided to the specied provider
     * A welcome is message is printed and provider is asked to choose their task
     * According to the task choice, provider bills chocAn, looksup for services, or asks the system to display provider directory
     */

    private void providerScreen(String code) {
        int choice;
        Provider myInfo;
        try {
            myInfo = new Provider(code, storageServer);
        } catch (FileNotFoundException e) {
            System.out.println("Provider not found.");
            return;
        }
        ProviderDirectory providerDirectory = new ProviderDirectory();
        do {
            System.out.println("\nWelcome, Provider " + myInfo.getName() + "!");
            System.out.println("1. Bill ChocAn Member");
            System.out.println("2. Display Service Directory");
            System.out.println("3. Lookup Services");
            System.out.println("4. Exit");
            System.out.print("Enter your choice: ");
            choice = scanner.nextInt();
            scanner.nextLine(); // Consume the newline character left by nextInt()
                        /**
                        * switch case for tasks of provider
                        * 
                        */
            switch (choice) {
                case 1:
                    System.out.print("Enter member number: ");
                    int memberNumber = scanner.nextInt();
                    scanner.nextLine(); // Consume the newline character left by nextInt()
                    Member myMember = null;
                    try {
                        myMember = new Member(memberNumber);
                        // myMember.addService(); // Implement the 'addService' method in the Member class
                    } catch (FileNotFoundException e) {
                        System.out.println("Member not found.");
                    }
                    System.out.println("Enter Service Date: MM-DD-YYYY");
                    String date = scanner.nextLine();
                    // Enter service code
                    System.out.println("Enter Service Code: ");
                    String service = providerDirectory.getService(scanner.nextLine());
                    myMember.addService(date, myInfo.getName(), service);
                    break;
                case 2:
                    System.out.println(providerDirectory.getAllServices());
                    break;
                case 3:
                    System.out.print("Enter service number: ");
                    int serviceNumber = scanner.nextInt();
                    scanner.nextLine(); // Consume the newline character left by nextInt()
                    try {
                        System.out.println(providerDirectory.getService(Integer.toString(serviceNumber)));
                    } catch (Exception e) {
                        System.out.println("Service not found.");
                    }
                    break;
                case 4:
                    System.out.println("Exiting...");
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        } while (choice != 4);
    }
    
      /**
     * managerScreen class is an interface class for the manager that helps the manager to interact with the system
     * @param code 9-digit long code provided to the specied manager
     * A welcome message is printed and manager is asked to choose their task
     * According to the task choice, manager creates provider report, member report, or their own report
     */
    private void managerScreen(String code) {
        int choice;
        Manager myInfo;
        System.out.print("Enter manager name: ");
        String name = scanner.nextLine();
        myInfo = new Manager(name, code, storageServer);
        do {
            System.out.println("\nWelcome, Manager " + myInfo.getName() + "!");
            System.out.println("1. Provider Report");
            System.out.println("2. Member Report");
            System.out.println("3. Manager Report");
            System.out.println("4. Exit");
            System.out.print("Enter your choice: ");
            choice = scanner.nextInt();
            scanner.nextLine(); // Consume the newline character left by nextInt()
            
             /**
                * switch case for tasks of manager
                * 
            */
            switch (choice) {
                case 1:
                    System.out.print("Enter provider number: ");
                    String providerNumber = scanner.nextLine();
                    Provider theProvider = null;
                    try {
                        theProvider = new Provider(providerNumber, storageServer);
                    } catch (FileNotFoundException e) {
                        System.out.println("Provider not found.");
                    }
                    myInfo.generateProviderReport(theProvider.getName());
                    break;
                case 2:
                    System.out.print("Enter member number: ");
                    String memberNumber = scanner.nextLine();
                    myInfo.generateMemberReport(memberNumber);
                    break;
                case 3:
                    myInfo.generateManagerReport();
                    break;
                case 4:
                    System.out.println("Exiting...");
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        } while (choice != 4);
    }

    public static void main(String[] args) {
        new MainScreen();
    }
}
