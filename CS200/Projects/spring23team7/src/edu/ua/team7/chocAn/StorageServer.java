package edu.ua.team7.chocAn;

import java.io.*;
import java.util.HashMap;

/**
 * Loads user data from storage.txt
 */
public class StorageServer {

    private static final String FILE_NAME = "storage.txt";
    private HashMap<String, Object> storageData;

    /**
     * Storage server initialization. Creates hashmap named storageData and loads data
     */
    public StorageServer() {
        storageData = new HashMap<>();
        loadData();
    }

    /**
     * loads data from storage.txt
     */
    void loadData() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(FILE_NAME));
            String line = br.readLine(); // skip header
            while ((line = br.readLine()) != null) {
                if (line.contains(" - ")) {
                    String[] tokens = line.split(" - ");
                    String userType = tokens[0];
                    String code = tokens[1];
                    storageData.put(userType + "_" + code, new Object());
                }
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the data from Storage.txt
     *
     * @param userType
     * @param code
     * @return userType and code
     */
    public Object getData(String userType, String code) {
        return storageData.get(userType + "_" + code);
    }
}
