package edu.ua.team7.chocAn;
/**
 * @author Avery
 *
 * VerifyOperator class verifies the identity of operator by looking for their 9 digit code in storage.txt
 */
public class VerifyOperator {

    private StorageServer storageServer;

    /**
     * Default Constructor
     * @param storageServer storage server is an object that deals with the information in storage.txt
     */
    public VerifyOperator(StorageServer storageServer) {
        this.storageServer = storageServer;
    }

     /**
     * verifyManager method looks the 9-digit code in storage.txt
     * @param code 
     * @return returns true if the 9-digit code is found, otherwise returns false
     */
    public boolean verifyOperator(String code) {
        if (code == null || code.isEmpty()) {
            return false;
        }

        Object operator = storageServer.getData("Operator", code);
        if (operator == null) {
            return false;
        }

        return isValidCode(code);
    }

    /**
     * isValidCode checks whether the code is valid or not, using the attributes of code like length and char type
     * @param code 
     * @return  returns true if the 9-digit code is valid, otherwise returns false
     */
    private boolean isValidCode(String code) {
        if (code.length() != 9) {
            return false;
        }

        for (int i = 0; i < code.length(); i++) {
            char c = code.charAt(i);
            if (!Character.isDigit(c)) {
                return false;
            }
        }

        return true;
    }
}
