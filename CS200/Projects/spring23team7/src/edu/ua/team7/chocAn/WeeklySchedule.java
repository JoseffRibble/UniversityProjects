package edu.ua.team7.chocAn;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Dylan Brearley
 */

/**
 * Creates logs.txt of from provided_services.csv every week
 */
public class WeeklySchedule {

    private static final String PROVIDED_SERVICES_FILE = "provided_services.csv";

    /**
     * Main runner for WeeklySchedule. Creates logs.txt from provided_services.csv
     *
     * @param args
     */
    public static void main(String[] args) {
        // Get the current date and time
        LocalDateTime now = LocalDateTime.now();

        // Check if logs.txt exists, create it if it doesn't
        File logsFile = new File("logs.txt");
        if (!logsFile.exists()) {
            try {
                logsFile.createNewFile();
            } catch (IOException e) {
                System.err.println("Error creating logs.txt: " + e.getMessage());
                System.exit(1);
            }
        }

        // Parse the last run timestamp from logs.txt
        LocalDateTime lastRun = null;
        try (BufferedReader reader = new BufferedReader(new FileReader(logsFile))) {
            String line = reader.readLine();
            if (line != null) {
                lastRun = LocalDateTime.parse(line, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            }
        } catch (IOException e) {
            System.err.println("Error reading logs.txt: " + e.getMessage());
            System.exit(1);
        }

        // Check if it's been more than a week since the last run, or if it's Friday
        if (lastRun == null || lastRun.plusWeeks(1).isBefore(now) || now.getDayOfWeek() == DayOfWeek.FRIDAY) {
            // Run member report, provider report, and manager report
            Set<Integer> memberIds = new HashSet<>();
            Set<String> providerNames = new HashSet<>();

            try (BufferedReader br = new BufferedReader(new FileReader(PROVIDED_SERVICES_FILE))) {
                String line;
                boolean header = true;
                while ((line = br.readLine()) != null) {
                    if (header) {
                        header = false;
                        continue;
                    }
                    String[] data = line.split(",");
                    memberIds.add(Integer.parseInt(data[0].trim()));
                    providerNames.add(data[7].trim());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            for (int memberId : memberIds) {
                MemberReport memberReport = new MemberReport();
                memberReport.getServicesForMember(memberId);
            }

            for (String providerName : providerNames) {
                ProviderReport providerReport = new ProviderReport();
                providerReport.getServicesForProvider(providerName);
            }

            ManagerReport managerReport = new ManagerReport();
            managerReport.getAllServices();

            System.out.println("Weekly Scheduler ran successfully.");

            // Update the last run timestamp in logs.txt
            try (FileWriter writer = new FileWriter(logsFile)) {
                writer.write(now.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            } catch (IOException e) {
                System.err.println("Error writing to logs.txt: " + e.getMessage());
                System.exit(1);
            }
        }
    }
}
