package edu.ua.team7.chocAn;

/**
 * @author Matthew
 *
 * VerifyManager class verifies the identity of manager by looking for their 9 digit code in storage.txt
 */
public class VerifyManager {

    private StorageServer storageServer;

    /**
     * Default Constructor
     * @param storageServer storage server is an object that deals with the information in storage.txt
     */
    public VerifyManager(StorageServer storageServer) {
        this.storageServer = storageServer;
    }

    /**
     * verifyManager method looks the 9-digit code in storage.txt
     * @param code 
     * @return returns true if the 9-digit code is found, otherwise returns false
     */
    public boolean verifyManager(String code) {
        if (code == null || code.isEmpty()) {
            return false;
        }

        Object manager = storageServer.getData("Manager", code);
        if (manager == null) {
            return false;
        }

        return true;
    }
}
