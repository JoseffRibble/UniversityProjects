package edu.ua.team7.chocAn;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

/**
  * ManageProvider class helps the operator class to manage provider
 * According to operator's usage, providers.csv and storage.txt is updated
 *
 * @author Sonam
 */
public class ManageProvider {

    private static final String PROVIDERS_FILE = "providers.csv";
    private static final String STORAGE_FILE = "storage.txt";

    private StorageServer storageServer;

     /**
     * Default Constructor
     * @param storageServer storage server is an object that deals with the information in storage.txt
     */
    public ManageProvider(StorageServer storageServer) {
        this.storageServer = storageServer;
    }

    /**
     * addProvider method is used to add provider's info to storage.txt and providers.csv
     * @param number
     * @param name
     * @param address
     * @param city
     * @param state
     * @param zip
     * @return  returns true if the info is successfully updated otherwise returns false
     */
    public boolean addProvider(String number, String name, String address, String city, String state, String zip) {
        try {
            BufferedWriter providersWriter = new BufferedWriter(new FileWriter(PROVIDERS_FILE, true));
            String newProvider = number + "," + name + "," + address + "," + city + "," + state + "," + zip;
            providersWriter.write("\n" + newProvider);
            providersWriter.close();

            BufferedWriter storageWriter = new BufferedWriter(new FileWriter(STORAGE_FILE, true));
            String newStorageEntry = "Provider - " + number;
            storageWriter.write("\n" + newStorageEntry);
            storageWriter.close();

            storageServer.loadData();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     *  updateProvider method is used to update provider's info to storage.txt and providers.csv
     * @param code
     * @param number
     * @param name
     * @param address
     * @param city
     * @param state
     * @param zip
     * @return returns true if the info is successfully updated otherwise returns false
     */

    public boolean updateProvider(String number, String name, String address, String city, String state, String zip) {
        try {
            List<String> providers = Files.readAllLines(Paths.get(PROVIDERS_FILE));
            boolean updated = false;
            for (int i = 0; i < providers.size(); i++) {
                String[] providerData = providers.get(i).split(",");
                if (providerData[0].equals(number)) {
                    providers.set(i, number + "," + name + "," + address + "," + city + "," + state + "," + zip);
                    updated = true;
                    break;
                }
            }

            if (updated) {
                Files.write(Paths.get(PROVIDERS_FILE), providers, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
            } else {
                return false;
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * removeProvider method is used to remove provider's info to storage.txt and providers.csv
     * @param code this is the unique code to scan the provider from storage.txt
     * @return returns true if the provider is successfully removed otherwise returns false
     */


    public boolean removeProvider(String code) {
        try {
            List<String> providers = Files.readAllLines(Paths.get(PROVIDERS_FILE));
            List<String> storage = Files.readAllLines(Paths.get(STORAGE_FILE));

            boolean removedFromProviders = providers.removeIf(line -> line.startsWith(code + ","));
            boolean removedFromStorage = storage.removeIf(line -> line.equals("Provider - " + code));

            if (removedFromProviders && removedFromStorage) {
                Files.write(Paths.get(PROVIDERS_FILE), providers, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
                Files.write(Paths.get(STORAGE_FILE), storage, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);

                storageServer.loadData();
            } else {
                return false;
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
