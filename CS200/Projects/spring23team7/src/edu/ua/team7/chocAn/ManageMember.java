package edu.ua.team7.chocAn;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

/**
 * ManageMember class helps the operator class to manage member
 * According to operator's usage, members.csv and storage.txt is updated
 *
 * @author Avery
 */

public class ManageMember {

    private static final String MEMBERS_FILE = "members.csv";
    private static final String STORAGE_FILE = "storage.txt";

    private StorageServer storageServer;


    /**
     * Default Constructor
     * @param storageServer storage server is an object that deals with the information in storage.txt
     */
    public ManageMember(StorageServer storageServer) {
        this.storageServer = storageServer;
    }

    /**
     * addMember method is used to add Member's info to storage.txt and members.csv
     * @param id
     * @param name
     * @param address
     * @param city
     * @param state
     * @param zip
     * @return returns true if the info is successfully updated otherwise returns false
     */
    public boolean addMember(String id, String name, String address, String city, String state, String zip) {
        try {
            BufferedWriter membersWriter = new BufferedWriter(new FileWriter(MEMBERS_FILE, true));
            String newMember = id + "," + name + "," + address + "," + city + "," + state + "," + zip;
            membersWriter.write("\n" + newMember);
            membersWriter.close();

            BufferedWriter storageWriter = new BufferedWriter(new FileWriter(STORAGE_FILE, true));
            String newStorageEntry = "Member - " + id;
            storageWriter.write("\n" + newStorageEntry);
            storageWriter.close();

            storageServer.loadData();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * updateMember method is used to update Member's info to storage.txt and members.csv
     * @param code
     * @param id
     * @param name
     * @param address
     * @param city
     * @param state
     * @param zip
     * @return returns true if the info is successfully updated otherwise returns false
     */
    public boolean updateMember(String id, String name, String address, String city, String state, String zip) {
        try {
            List<String> members = Files.readAllLines(Paths.get(MEMBERS_FILE));
            boolean updated = false;
            for (int i = 0; i < members.size(); i++) {
                String[] memberData = members.get(i).split(",");
                if (memberData[0].equals(id)) {
                    members.set(i, id + "," + name + "," + address + "," + city + "," + state + "," + zip);
                    updated = true;
                    break;
                }
            }

            if (updated) {
                Files.write(Paths.get(MEMBERS_FILE), members, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
            } else {
                return false;
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * removeMember method is used to remove member's info to storage.txt and members.csv
     * @param code this is the unique code to scan the member from storage.txt
     * @return returns true if the member is successfully removed otherwise returns false
     */
    public boolean removeMember(String code) {
        try {
            List<String> members = Files.readAllLines(Paths.get(MEMBERS_FILE));
            List<String> storage = Files.readAllLines(Paths.get(STORAGE_FILE));

            boolean removedFromMembers = members.removeIf(line -> line.startsWith(code + ","));
            boolean removedFromStorage = storage.removeIf(line -> line.equals("Member - " + code));

            if (removedFromMembers && removedFromStorage) {
                Files.write(Paths.get(MEMBERS_FILE), members, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
                Files.write(Paths.get(STORAGE_FILE), storage, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);

                storageServer.loadData();
            } else {
                return false;
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
