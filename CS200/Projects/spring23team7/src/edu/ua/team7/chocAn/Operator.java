package edu.ua.team7.chocAn;

/**
 * Operator Class
 */
public class Operator {
    private String id;
    private ManageMember manageMember;
    private ManageProvider manageProvider;

    /**
     * Operator constructor
     *
     * @param id
     * @param storageServer
     */
    public Operator(String id, StorageServer storageServer) {
        this.id = id;
        this.manageMember = new ManageMember(storageServer);
        this.manageProvider = new ManageProvider(storageServer);
    }

    /**
     * get Operator id function
     *
     * @return operator's function
     */
    public String getId() {
        return this.id;
    }

    /**
     * get the ManageMember class associated with operator
     *
     * @return manageMember
     */
    public ManageMember getManageMember() {
        return this.manageMember;
    }

    /**
     * get the ManageProvider class associated with operator
     *
     * @return manageProvider
     */
    public ManageProvider getManageProvider() {
        return this.manageProvider;
    }
}
