package edu.ua.team7.chocAn;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Manages and loads list of services from directory.csv
 */
public class ProviderDirectory {
    private static final String DIRECTORY_FILE = "directory.csv";
    private Map<String, String> services;

    /**
     * Initialize provider directory by creating a new hashmap and loading services
     */
    public ProviderDirectory() {
        services = new HashMap<>();
        loadServices();
    }

    /**
     * load services from directory.csv
     */
    private void loadServices() {
        try (BufferedReader br = new BufferedReader(new FileReader(DIRECTORY_FILE))) {
            String line;
            boolean header = true;
            while ((line = br.readLine()) != null) {
                if (header) {
                    header = false;
                    continue;
                }
                String[] data = line.split(", ");
                services.put(data[0].trim(), data[1].trim());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * get services from list of services given service code
     *
     * @param serviceCode
     * @return service.get(serviceCode)
     */
    public String getService(String serviceCode) {
        return services.get(serviceCode);
    }

    /**
     * returns all services in provider directory
     *
     * @return Provider directory \n =========== \n list of service keys and values
     */
    public String getAllServices() {
        StringBuilder sb = new StringBuilder();
        sb.append("Provider Directory\n");
        sb.append("=============\n");
        for (Map.Entry<String, String> entry : services.entrySet()) {
            sb.append(entry.getKey()).append(" ").append(entry.getValue()).append("\n");
        }
        return sb.toString();
    }
}
