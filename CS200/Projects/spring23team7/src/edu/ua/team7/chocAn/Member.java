package edu.ua.team7.chocAn;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Member Class
 */
public class Member {
    private String name, address, city, state;
    private int ZIP, number;
    private Services[] services = new Services[50];

    /**
     * Initialize member their given id number. Takes member values from members.csv
     *
     * @param number
     * @throws FileNotFoundException
     */
    public Member(int number) throws FileNotFoundException {
        File file = new File("members.csv");
        Scanner scanner = new Scanner(file);
        scanner.nextLine(); // skip the header row
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] fields = line.split(",");
            int id = Integer.parseInt(fields[0]);
            if (id == number) {
                this.name = fields[1];
                this.address = fields[2];
                this.city = fields[3];
                this.state = fields[4];
                this.ZIP = Integer.parseInt(fields[5]);
                this.number = id;
                break;
            }
        }
        scanner.close();
        getServices(number);
    }

    /**
     * Grabs list of services for a member from provided_services.csv
     *
     * @param number
     * @throws FileNotFoundException
     */
    public void getServices(int number) throws FileNotFoundException {
        File file = new File("provided_services.csv");
        Scanner scanner = new Scanner(file);
        scanner.nextLine(); // skip the header row
        int index = 0; // index for adding services to array
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] fields = line.split(",");
            int id = Integer.parseInt(fields[0]);
            if (id == number) {
                services[index] = new Services(this, fields[6], fields[7], fields[8]);
                index++;
            }
        }
        scanner.close();
    }

    /**
     * Adds service to provided_services.csv storage
     *
     * @param serviceDate
     * @param providerName
     * @param serviceName
     */
    public void addService(String serviceDate, String providerName, String serviceName) {
        String newService = String.join(",", String.valueOf(number), name, address, city, state, String.valueOf(ZIP), serviceDate, providerName, serviceName);

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter("provided_services.csv", true));
            bw.newLine();
            bw.write(newService);
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get member name function
     *
     * @return member's name
     */
    public String getName() {
        return name;
    }

    /**
     * get member's address function
     * @return member's address
     */
    public String getAddress() {
        return address;
    }

    /**
     * get member's city function
     *
     * @return member's city
     */
    public String getCity() {
        return city;
    }

    /**
     * get member's state function
     * @return member's state
     */
    public String getState() {
        return state;
    }

    /**
     * get member's zip function
     *
     * @return member's zip
     */
    public int getZIP() {
        return ZIP;
    }

    /**
     * get member's number function
     * @return member's number
     */
    public int getMemberNum() {
        return number;
    }

    /**
     * get list of member's services function
     * @return services list
     */
    public Services[] getServices() {
        return services;
    }
}
