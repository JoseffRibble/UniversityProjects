package edu.ua.team7.chocAn;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Generates Provider report from provided_services,csv
 */
public class ProviderReport {

    private static final String PROVIDED_SERVICES_FILE = "provided_services.csv";

    /**
     * get list of services for generating provider report
     *
     * @param providerName
     * @return list of strings with provider services
     */
    public List<String> getServicesForProvider(String providerName) {
        List<String> providerServices = new ArrayList<>();
        
        try (BufferedReader br = new BufferedReader(new FileReader(PROVIDED_SERVICES_FILE))) {
            String line;
            boolean header = true;
            while ((line = br.readLine()) != null) {
                if (header) {
                    header = false;
                    continue;
                }
                String[] data = line.split(",");
                if (data[7].trim().equals(providerName)) {
                    providerServices.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String OUTPUT_FILE = "providerReport"+ providerName.replace(" ", "") + ".txt";
        writeServicesToFile(providerServices, OUTPUT_FILE);
        return providerServices;
    }

    /**
     * Write list of services to OUTPUT_FILE
     *
     * @param services
     * @param OUTPUT_FILE
     */
    public void writeServicesToFile(List<String> services, String OUTPUT_FILE) {
        try (PrintWriter writer = new PrintWriter(OUTPUT_FILE)) {
            for (String service : services) {
                writer.println(service);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * main runner for generating provider report
     *
     * @param args
     */
    public static void main(String[] args) {
        ProviderReport providerReport = new ProviderReport();

        // You can replace this part with the actual provider's code from your application
        String providerCode = "123456789";
        Provider provider;
        try {
            provider = new Provider(providerCode, new StorageServer());
        } catch (FileNotFoundException e) {
            System.out.println("Provider not found.");
            return;
        }
        String providerName = provider.getName();

        List<String> services = providerReport.getServicesForProvider(providerName);
        for (String service : services) {
            System.out.println(service);
        }
    }
}
