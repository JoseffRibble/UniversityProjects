package edu.ua.team7.chocAn;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Generates reports for managers from provided_services.csv to managerReport.txt
 */
public class ManagerReport {

    private static final String PROVIDED_SERVICES_FILE = "provided_services.csv";
    private static final String OUTPUT_FILE = "managerReport.txt";

    /**
     * Returns a list of services from storage
     * @return String List of services
     */
    public List<String> getAllServices() {
        List<String> allServices = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(PROVIDED_SERVICES_FILE))) {
            String line;
            boolean header = true;
            while ((line = br.readLine()) != null) {
                if (header) {
                    header = false;
                    continue;
                }
                allServices.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        writeServicesToFile(allServices);
        return allServices;
    }

    /**
     * Takes input list of strings and writes to storage
     *
     * @param services
     */
    public void writeServicesToFile(List<String> services) {
        try (PrintWriter writer = new PrintWriter(OUTPUT_FILE)) {
            for (String service : services) {
                writer.println(service);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * main runner for creating manager reports
     *
     * @param args
     */
    public static void main(String[] args) {
        ManagerReport managerReport = new ManagerReport();
        List<String> services = managerReport.getAllServices();
        managerReport.writeServicesToFile(services);
        for (String service : services) {
            System.out.println(service);
        }
    }
}
