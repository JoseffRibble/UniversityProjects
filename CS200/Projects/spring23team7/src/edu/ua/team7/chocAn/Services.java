package edu.ua.team7.chocAn;

/**
 * Services Class
 *
 * @author Joseff Ribble
 */
public class Services {
    private Member member;
    private String service_date;
    private String provider_name, service_name;

    /**
     * Initialize services
     *
     * @param member
     * @param service_date
     * @param provider_name
     * @param service_name
     */
    public Services(Member member, String service_date, String provider_name, String service_name) {
        this.member = member;
        this.service_date = service_date;
        this.provider_name = provider_name;
        this.service_name = service_name;
    }

    /**
     * Get Member Name
     *
     * @return member name
     */
    public String getMemberName() {
        return member.getName();
    }

    /**
     * Get member address
     *
     * @return member address
     */
    public String getMemberAddress() {
        return member.getAddress();
    }

    /**
     * Get member city
     *
     * @return member city
     */
    public String getMemberCity() {
        return member.getCity();
    }

    /**
     * Get member state
     *
     * @return member state
     */
    public String getMemberState() {
        return member.getState();
    }

    /**
     * Get member ZIP
     *
     * @return member ZIP
     */
    public int getMemberZIP() {
        return member.getZIP();
    }

    /**
     * Get member number
     *
     * @return member Number
     */
    public int getMemberNumber() {
        return member.getMemberNum();
    }

    /**
     * Get service date
     *
     * @return member's service date
     */
    public String getServiceDate() {
        return service_date;
    }

    /**
     * Get provider's name who works with member
     *
     * @return provider's name
     */
    public String getProviderName() {
        return provider_name;
    }

    /**
     * Get service name
     *
     * @return service name
     */
    public String getServiceName() {
        return service_name;
    }
}
