package edu.ua.team7.chocAn;

/**
 * Manager Class
 */
public class Manager {
    private String name;
    private String id;
    private ManageMember manageMember;
    private ManageProvider manageProvider;

    /**
     * Initializes Manager
     *
     * @param name
     * @param id
     * @param storageServer
     */
    public Manager(String name, String id, StorageServer storageServer) {
        this.name = name;
        this.id = id;
        this.manageMember = new ManageMember(storageServer);
        this.manageProvider = new ManageProvider(storageServer);
    }

    /**
     * get the Manager's name function
     *
     * @return Manager's name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the Manager's name
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * get the Manager id
     *
     * @return Manager id
     */
    public String getId() {
        return id;
    }

    /**
     * sets the Manager's id
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    // Wrapper methods for ManageMember

    /**
     * adds a Member to storage
     *
     * @param id
     * @param name
     * @param address
     * @param city
     * @param state
     * @param zip
     * @return manageMember.addMember
     */
    public boolean addMember(String id, String name, String address, String city, String state, String zip) {
        return manageMember.addMember(id, name, address, city, state, zip);
    }

    /**
     * updates a member in storage
     *
     * @param code
     * @param id
     * @param name
     * @param address
     * @param city
     * @param state
     * @param zip
     * @return manageMember.updateMember
     */
    public boolean updateMember(String id, String name, String address, String city, String state, String zip) {
        return manageMember.updateMember(id, name, address, city, state, zip);
    }

    /**
     * Removes a member from storage
     *
     * @param code
     * @return manageMember.removeMember
     */
    public boolean removeMember(String code) {
        return manageMember.removeMember(code);
    }

    // Wrapper methods for ManageProvider

    /**
     * Adds a provider to storage
     *
     * @param number
     * @param name
     * @param address
     * @param city
     * @param state
     * @param zip
     * @return manageProvider.addProvider
     */
    public boolean addProvider(String number, String name, String address, String city, String state, String zip) {
        return manageProvider.addProvider(number, name, address, city, state, zip);
    }

    /**
     * Updates provider in storage
     *
     * @param code
     * @param number
     * @param name
     * @param address
     * @param city
     * @param state
     * @param zip
     * @return manageProvider.updateProvider
     */
    public boolean updateProvider(String number, String name, String address, String city, String state, String zip) {
        return manageProvider.updateProvider(number, name, address, city, state, zip);
    }

    /**
     * Removes a provider from storage
     *
     * @param code
     * @return manageProvider.removeProvider(code)
     */
    public boolean removeProvider(String code) {
        return manageProvider.removeProvider(code);
    }

    /**
     * Generates a provider report
     *
     * @param providerName
     */
    public void generateProviderReport(String providerName) {
        ProviderReport providerReport = new ProviderReport();
        System.out.println(providerReport.getServicesForProvider(providerName));
    }

    // Method for ManagerReport

    /**
     * Generates the manager report
     */
    public void generateManagerReport() {
        ManagerReport managerReport = new ManagerReport();
        System.out.println(managerReport.getAllServices());
    }

    /**
     * Generates a member report
     *
     * @param memberId
     */
    public void generateMemberReport(String memberId) {
        MemberReport memberReport = new MemberReport();
        System.out.println(memberReport.getServicesForMember(Integer.parseInt(memberId)));
    }
}
