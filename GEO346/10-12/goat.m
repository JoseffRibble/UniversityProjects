function win=goat(strategy)
  % switch: strategy = 1.
  % stay: strategy = 0

  % Let's set the prize door always to 1 to make things easier here.
  % It doesn't matter if everything else can vary randomly
  prize = 1;

  % Choose a random door
  selected = randi(3,1); % set this to a random integer between 1 and 3

  % Now the showmaster opens a door that is not your door and not the prize.
  if selected == 1
    remaining = 2; 
  elseif selected == 2
    remaining = 1;
  else
    remaining = 1;
  end

  % If your strategy is to switch, then you need to take the 
  % remaining door, which is not the one you currently pick
  if strategy
    ind = remaining ~= selected; % The index of the remaining door that is not the one you currently choose
    selected = remaining(ind);
  end

  if selected == prize
    win = 1;
  else
    win = 0;
  end
  
      
  
