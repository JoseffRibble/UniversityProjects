function [r,xfinal] = logisticMap(npoints)

  n = 1000;
  
  for k=1:npoints
    x1 = rand;
    r(k) = 4*rand;
%%% put here your function for the logistic equation such that
%%% the input is x1 for the starting year, r(k) for the biotic
%%% potential and n for the number of iterations (number of years)
    x = logeq(x1,r(k),n);


    xfinal(k) = x(end);
  end