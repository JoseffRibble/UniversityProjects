function x = logeq(x1, r, n)
    x(1) = x1;
    for j = 2:n
        x(j) = r * x(j-1) * (1 - x(j-1));
    end