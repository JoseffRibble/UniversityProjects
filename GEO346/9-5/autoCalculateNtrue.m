function Nbest = autoCalculateNtrue(Nobs, t)
  %calculateNtrue - Find the closest true value of N, automatically generates testing values.6
  %
  % Syntax: Ntest .* exp(-Ntest * t);
  %
  % Input:
  % Nobs - observed N value
  % t - time
  % 
  % Output:
  % Nbest - The best possible values from given Ncalc vector
  % 
  % Author Joseff Ribble 
  % Website: https://codeberg.org/JoseffRibble
  % Last edited: 2023-9-5
  % Made with GNU Octave
  
  % Generate testing values
  Ntest = randn(1,1000) .* 3 .+ Nobs;
  
  % Calculate Ncalc assuming test variables are true
  Ncalc = Ntest .* exp(-Ntest * t);
  
  % Determine which value of Ncalc is the closest to Nobs
  [f, ind] = min(abs(Ncalc .- Nobs));  % Subtract Nobs from Ncalc, find the abs, and find the min
  Nbest = Ntest(ind);  % Find the closest value at the index of the minimum value
  
  
 % Testing Variables:
 % Nobs = 300
 % Ntest = [280:0.1:320]
 % t = 20*10^(-6)