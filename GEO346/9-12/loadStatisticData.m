function loadStatisticData(file)
%loadStatisticData Reads statistic data from file. CSV must only contain
%numerics


M = csvread(file, 1);
data.massEm.values = M(:,3);
data.massEm.unit = "E-m";
data.radius.values = M(:,4);
data.radius.unit = "km";
data.gravity.values = M(:,6);
data.gravity.unit = "g";

%massEm
disp("massEm Maximum:");
disp(join([max(data.massEm.values),data.massEm.unit]));
disp("massEm Minimum:");
disp(join([min(data.massEm.values),data.massEm.unit]));
disp("massEm mean:");
disp(join([mean(data.massEm.values),data.massEm.unit]));
%radius
disp("radius Maximum:");
disp(join([max(data.radius.values),data.radius.unit]));
disp("radius Minimum:");
disp(join([min(data.radius.values),data.radius.unit]));
disp("radius mean:");
disp(join([mean(data.radius.values),data.radius.unit]));
%gravity
disp("gravity Maximum:");
disp(join([max(data.gravity.values),data.gravity.unit]));
disp("gravity Minimum:");
disp(join([min(data.gravity.values),data.gravity.unit]));
disp("gravity mean:");
disp(join([mean(data.gravity.values),data.gravity.unit]));

end

