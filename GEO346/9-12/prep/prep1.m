load('grainsize.mat');

sand = [gs(:,1)];
silt = [gs(:,2)];
clay = [gs(:,3)];

samples.sand.values = sand;
samples.sand.unit = '%';
samples.silt.values = silt;
samples.silt.unit = '%';
samples.clay.values = clay;
samples.clay.unit = '%';

