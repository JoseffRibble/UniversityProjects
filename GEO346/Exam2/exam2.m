% Exam 2
%
% Question 1:
% a)
clear
close all

Q1x = [-0.3, 1.22, 3.94, 4.5];
Q1y = [12, 1, 9.9, -2.3];

plot(Q1x,Q1y,'o');
hold on;
linear = interp1(Q1x,Q1y,5,'linear','extrap');
plot(5,linear,'x')
spline = interp1(Q1x,Q1y,5,'spline','extrap');
plot(5,spline,'+')
pchip = interp1(Q1x,Q1y,5,'pchip','extrap');
plot(5,pchip,'d')

% Results:
% linear: -13,1929
% spline: -21.4649
% pchip: -2.4475
%
% b)
Q1P = polyfit(Q1x,Q1y,1);
Q1Y = polyval(Q1P,5);

plot(5,Q1Y,'^')

title('Question 1');
xlabel('x value');
ylabel('y value');
legend('Measured','Linear','Spline','Pchip','polyval')
hold off

% c)
% Given that the data is decreasing, then increasing, then decreasing
% again, I would conclude that this pattern would continue. This would make 
% the most trustworthy result the value from polyval. Polyfit at degree one
% creates a linear approximation of the points, giving an approximation in
% the general direction of all the data together. Extrapolation uses
% interpolation to predict what the next value can be, but this only uses
% the relationship between the last two points, not the data as a whole.
% This doesn't get the whole picture.

% Question 2:
% a)
Q2a = fminsearch(@(x) abs(somfun(0.5, x)-2),2);
% result: 1.5317

% b)
Q2b = fminsearch(@(x) abs(somfun(0.5, x)-2),4); 
% result: 4.7515

% c)
% Two different values are obtained because somfun is a nonlinear function.
% fminsearch looks for a local minima, in which somfun has multiple. 2 is
% closer to the minima around 1.5 and 4 is closer to the 4.8 minima.
Q2x = 0:0.01:10;
Q2y = somfun(0.5,Q2x);
figure
plot(Q2x,Q2y)
xlabel('x values')
ylabel('result of somfun(0.5,x)')
title('somfun visualization')

% Question 3:
% I'm going to use an example from one of my favourite video games,
% Elite:Dangerous. E:D is a space flight simulator with a procedurally
% generated 1:1 recreation of the Milky Way. It takes place around the year
% 3309 shortly after humans discovered FTL travel. My areas of interest are
% in the exploration side of the game as only a tiny portion the galaxy
% has been visited. The game outputs lots of useful info into a .txt file
% as you observe certain things. I use tools created by the community to
% upload my data and get the data from thousands of others. Last year, my
% squad and I hosted an expedition in search of an elusive green gas giant
% that was posted many years ago on the forums. This was before modern
% catalogue tools existed so we needed to rediscover it so we can add it to
% the galactic catalogue. We used lots of information to do this, including
% past green gas giant info, star system mass codes, and even screenshots
% of the skybox to try and find the exact boxel that it was located in (the
% game uses cubes to save from rendering every possible combination of stars
% in the sky). Using the vast collection of previous data, as well as new
% data as we collected it, we were able to roughly track where this person
% was at the time of posting and comb through relevent stars until we
% eventually found it. Without the information we had today, we would never
% have rediscovered one body among trillions of stars. There's still so
% much more that can be done though. For example taking information about
% discovered biologicals in an attempt to find more or using market data to
% find optimal trade routes. 
