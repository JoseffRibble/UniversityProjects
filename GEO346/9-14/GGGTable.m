function GreenGasGiants = GGGTable(filename)

% Option configs
opts = delimitedTextImportOptions("NumVariables", 41);
opts.DataLines = [2,inf];
opts.Delimiter = ",";

% Specify column names and types
opts.VariableNames = ["System", "Planet", "MassCode", "Region", "GECEntryOrScreenshot", "StarClasses", "MainStar", "SoloStar", "GasGiantType", "MassEm", "Radiuskm", "TemperatureK", "Gravity", "OrbPeriodd", "SmAAU", "OrbEcc", "OrbIncldeg", "ArgPedeg", "RotPeriodd", "AxialTiltdeg", "Ringed", "NoOfMoons", "ForumReport", "LikelyDiscoverer", "IngameFDTag", "EDSMUploader", "EDSMDate", "CodexReporter", "CodexDate", "DistFromSolLY", "CoordX", "CoordY", "CoordZ", "AtmH2", "AtmHe", "AtmO2", "AtmH2O", "AtmNH3", "AtmCH4", "MainStarAgeMy", "MedianAgeOfType"];
opts.VariableTypes = ["string", "double", "categorical", "double", "string", "string", "categorical", "categorical", "categorical", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "categorical", "double", "datetime", "string", "string", "string", "datetime", "string", "datetime", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double"];

GreenGasGiants = readtable(filename, opts);

end