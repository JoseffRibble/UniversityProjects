#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  2 15:58:09 2023

@author: joseff
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import griddata

data = np.loadtxt('Hawaii.txt', delimiter=',')

x = data[:,0]
y = data[:,1]
z = data[:,2]

xi = np.arange(np.min(x), np.max(x), 0.1)
yi = np.arange(np.min(y), np.max(y), 0.1)
[Xi,Yi] = np.meshgrid(xi,yi)

Zi = griddata((x,y),z, (Xi,Yi),'linear')

ax = plt.figure().add_subplot(projection = '3d')

ax.plot_surface(Xi,Yi,Zi)
ax.set_xlabel('Latitude')
ax.set_ylabel('Longitude')
ax.set_zlabel('Elevation')
plt.savefig('all.pdf')


plt.clf()
ind = z<0
z[ind] = 0

xi = np.arange(np.min(x), np.max(x), 0.1)
yi = np.arange(np.min(y), np.max(y), 0.1)
[Xi,Yi] = np.meshgrid(xi,yi)

Zi = griddata((x,y),z, (Xi,Yi),'linear')

ax = plt.figure().add_subplot(projection = '3d')

ax.plot_surface(Xi,Yi,Zi)
ax.set_xlabel('Latitude')
ax.set_ylabel('Longitude')
ax.set_zlabel('Elevation')
plt.savefig('above0.pdf')