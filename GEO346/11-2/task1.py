#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  2 15:28:12 2023

@author: joseff
"""

import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('MAVEN-Mars-2023-5-14-position-subsampled.txt', delimiter=',')

x = data[:,0]
y = data[:,1]
z = data[:,2]

plt.clf()
plt.plot(x,y,'.')
plt.xlabel('x values')
plt.ylabel('y values')
plt.savefig('xy.pdf')

plt.clf()
plt.plot(x,z,'.')
plt.xlabel('x values')
plt.ylabel('z values')
plt.savefig('xz.pdf')

plt.clf()
plt.plot(y,z,'.')
plt.xlabel('y values')
plt.ylabel('z values')
plt.savefig('yz.pdf')

