function calculatePi(N)
  %calculatePi - Calculate Pi using the summation function 
  %
  % Syntax: sum( 4 * ((-1) .^ v) ./ (2 * v + 1) )
  %
  % Input:
  % N - The size of the vector given to the summation function
  % 
  % Output:
  % pi - The approximation of Pi
  % 
  % Author Joseff Ribble 
  % Website: https://codeberg.org/JoseffRibble
  % Last edited: 2023-8-31

  %-----------BEGIN CODE--------------
  
  v = 0:N; % Erstelle ein Vektor von 0 bis N
  
  pi =sum( 4 * ((-1) .^ v) ./ (2 * v + 1) ) % Summation Funktion
  
  %------------END CODE---------------
  
  % What I noticed:
  %   -As N became larger, the value became closer and closer to pi. Like an inverse exponentional function.
  %   -The script writes variables to the workspace while the function does not.
  % Testing Method: Run calculatePi(N) in Command Window where N is the value to test.