%% Import data from text file
% Script for importing data from the following text file:
%
%    filename: /home/joseff/Documents/uni/GEO346/Exam1/McClymont-glacial(1).tab
%
% Auto-generated by MATLAB on 19-Sep-2023 16:48:35

%% Set up the Import Options and import the data
opts = delimitedTextImportOptions("NumVariables", 15);

% Specify range and delimiter
opts.DataLines = [62, Inf];
opts.Delimiter = "\t";

% Specify column names and types
opts.VariableNames = ["Event", "Event1", "Latitude", "Longitude", "DateTime", "Site", "Proxy", "MIS", "AgeMaMinimum", "AgeMaMaximum", "Durationka", "SSTCMean", "SSTStdDev", "NoNumberOfDataPoints", "TempResTimeResolutionBetweenData"];
opts.VariableTypes = ["double", "categorical", "double", "double", "datetime", "double", "categorical", "double", "double", "double", "double", "double", "double", "double", "double"];

% Specify file level properties
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";

% Specify variable properties
opts = setvaropts(opts, ["Event1", "Proxy"], "EmptyFieldRule", "auto");
opts = setvaropts(opts, "DateTime", "InputFormat", "yyyy-MM-dd");
opts = setvaropts(opts, ["Event", "Site"], "TrimNonNumeric", true);
opts = setvaropts(opts, ["Event", "Site"], "ThousandsSeparator", ",");

% Import the data
McClymontglacial1 = readtable("/home/joseff/Documents/uni/GEO346/Exam1/McClymont-glacial(1).tab", opts);


%% Clear temporary variables
clear opts