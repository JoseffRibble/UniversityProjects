import numpy as np

grades = np.array([[100, 80, 70, 90],[90, 100, 80, 90], [93, 85, 90, 90]])
print(np.average(grades[:, 0]))
print(np.average(grades[:, 2]))
print(np.average(grades[0]))