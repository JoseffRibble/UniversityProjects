% Setup
clear
close all
load("Hawaii.mat");

% Part 1
xreg = 17:0.1:25;
yreg = -162:0.1:-153;
[gridX,gridY] = meshgrid(yreg,xreg);
grid1 = griddata(longitude,latitude,elevation,gridX,gridY,'nearest');
figure
surf(gridX,gridY,grid1)
xlabel('longitude');
ylabel('latitude');
title("Height data around the Hawai'ian Islands")

% Part 2
elevation2 = elevation;
elevation2(elevation2 < 0) = 0;
grid2 = griddata(longitude,latitude,elevation2,gridX,gridY);
figure
surf(gridX,gridY,grid2)
xlabel('longitude');
ylabel('latitude');
title("Hawai'ian Islands above sea level")

% Part 3
elevation3 = elevation;
elevation3(elevation3 > 0) = 0;
grid3 = griddata(longitude,latitude,elevation3,gridX,gridY,'cubic');
figure
surf(gridX,gridY,grid3)
xlabel('longitude');
ylabel('latitude');
title("Hawai'ian Islands bathymetry")