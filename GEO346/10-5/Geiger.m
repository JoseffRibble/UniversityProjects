function Ntrue = Geiger(Ncount)
    Ntrue = fminsearch(@(x) GeigerMisfit(Ncount, x),Ncount);
end

% Geiger(800) = 813.1162
% The geiger function takes an imput of Ncount and calculates the true
% value for counted gamma rays using the formula $N_obs = N_true * e^(-N_true * t)$.
% It uses fminsearch to find the minimum value along the function
% GeigerMisfit starting at guess Ncount. 