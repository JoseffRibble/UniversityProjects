function misf = GeigerMisfit(Ncount, Ntrue)
    t = 20e-6;
    Nobs = Ntrue * exp(-Ntrue * t);
    
    misf = abs(Ncount - Nobs);

end