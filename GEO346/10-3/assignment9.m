close all;

%Part 1
load("BlackWarrior-Missing.mat");

tint = datetime(2022,12,14,10,00,00):minutes(15):datetime(2022,12,15,23,59,00);

plot(t,h,'k')
hold on

interpolated = interp1(t,h,tint,'linear');
plot(tint,interpolated,'r')
interpolated = interp1(t,h,tint,'nearest');
plot(tint,interpolated,'g')
interpolated = interp1(t,h,tint,'spline');
plot(tint,interpolated,'b')
interpolated = interp1(t,h,tint,'pchip');
plot(tint,interpolated,'m')

xlabel("Date");
ylabel("Height")
title("Height of the Black Warrior River")


%Part 2
bw = readtable("BlackWarrior.txt");
time = bw.datetime;
gheight = bw.x3093_00065;
rangeInd = time >= datetime(2022,12,12) & time <= datetime(2022,12,18);

plot(time(rangeInd),gheight(rangeInd),'--')

legend('data','linear','nearest','spline','pchip','Real data')
