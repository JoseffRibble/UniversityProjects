#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 15:39:05 2023

@author: joseff
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def assignment16(filename, figname):
    
    plt.clf()
    data = pd.read_table(filename, sep=',')
    
    dday = data["dday"].values
    x = data["x"].values
    y = data["y"].values
    z = data["z"].values
    
    plt.plot(dday,x,'2')
    plt.plot(dday,y,'2')
    plt.plot(dday,z,'2')
    plt.xlabel('Time (Decimal Day)')
    plt.ylabel('X, Y, & Z values')
    
    missing_time_vals = np.arange(134.498860122, 134.630804543, 0.001)
    
    cx = np.polyfit(dday,x,4)
    est_x = np.polyval(cx,missing_time_vals)
    plt.plot(missing_time_vals,est_x,'-', color='tab:blue')
    
    cy = np.polyfit(dday,y,6)
    est_y = np.polyval(cy,missing_time_vals)
    plt.plot(missing_time_vals,est_y,'-', color='tab:orange')
    
    cz = np.polyfit(dday,z,9)
    est_z = np.polyval(cz,missing_time_vals)
    plt.plot(missing_time_vals,est_z,'-', color='tab:green')
    
    missing_data = pd.read_table("Mars-missing.txt", sep=',')
    missing_x = missing_data["x"].values
    missing_y = missing_data["y"].values
    missing_z = missing_data["z"].values
    missing_ddays = missing_data["dday"].values
    plt.plot(missing_ddays,missing_x,'.', color='tab:blue')
    plt.plot(missing_ddays,missing_y,'.', color='tab:orange')
    plt.plot(missing_ddays,missing_z,'.', color='tab:green')
    
    plt.title('X, Y, & Z over time with interpolated and real data')
    plt.legend(['X','Y','Z','X intp','Y intp','Z intp','real X','real Y','real Z'])
    plt.savefig(figname)
    
assignment16("Mars-cut.txt", "Mars-cut.pdf")
# I went with degrees 4, 6, and 9 respectibly. Depending on the value, the polynomial
# must increase or decrease at the ends, resulting in some values being much more
# incorrect than others. The start and end points aren't perfect.

assignment16("Mars-cut-withfew.txt", "Mars-cut-withfew.pdf")
# The inclusion of just a few more points definitely makes a difference. 
# I used the same degrees and all the values became closer to the truth.
# The Z interpolation is the most notible, becoming near perfect while the other
# two moved towards the actual points.
    
