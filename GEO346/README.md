# GEO346 - Computers in Geology

Collection of various assignments done in GEO346. Assignments involved using either MATLAB or Python to manipulate and visualise data for research purposes.
