#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 31 15:27:44 2023

@author: joseff
"""

import numpy as np
import scipy.optimize as so


def misfit(Ntrue, Ncount):
    Nobs = Ntrue * np.exp(-Ntrue * 0.00002)
    return abs(Ncount - Nobs)
    

def solve(Ncount):
    return so.fmin(lambda x : misfit(x, Ncount), Ncount)


'''
Geiger.solve(800)
The output is an array containing 813.1162262
'''