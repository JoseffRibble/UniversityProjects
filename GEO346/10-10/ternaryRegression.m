function ternaryRegression
  
  % load the data, assign the correct columns to the variables "depth", "sand", "silt" and "clay"
  %%%% EDIT HERE
  data = readtable("Lakegrainsize.xls");
  depth = data.Depth_m_;
  sand = data.Sand_proportion_;
  silt = data.Silt_proportion_;
  clay = data.Clay_proportion_;

  % These lines create a polynomial fit between depth and the sand/clay ratio
  % as well as between the depth and the silt/clay ratio
  coef_sand_clay = polyfit(log(depth), log(sand./clay),1);
  coef_silt_clay = polyfit(log(depth), log(silt./clay),1);

  % define the depths
  alldepths = 0:0.1:150;

  % These two lines evaluate the regression of the sand/clay ratio depending on the depth
  % as well as for the silt/clay ratio depending on the depth
  log_reg_sand_clay = polyval(coef_sand_clay, log(alldepths));
  log_reg_silt_clay = polyval(coef_silt_clay, log(alldepths));

  % These three lines evaluate the regression coordinates for sand, silt, and clay, based
  % on the sand/clay ratios and the silt/clay ratios.
  sand_reg = exp(log_reg_sand_clay)./( exp(log_reg_sand_clay)  + exp(log_reg_silt_clay) + 1 );
  silt_reg = exp(log_reg_silt_clay)./( exp(log_reg_sand_clay)  + exp(log_reg_silt_clay) + 1 );
  clay_reg = 1./( exp(log_reg_sand_clay) + exp(log_reg_silt_clay) + 1 );

  %%%% EDIT HERE
  % Plot here the ternary regression for sand,silt,clay and the corresponding depth.
  % The sand values of the ternary regression are in the variable sand_reg,
  % the silt values of the ternary regression are in the variable silt_reg,
  % the clay values of the ternary regression are in the variable clay_reg.
  terplot
  ternaryc(sand_reg, silt_reg, clay_reg, alldepths,'.');
  hold on
  [hd, hcb] = ternaryc(sand, silt, clay, depth,'o');
  terlabel('Sand', 'Silt', 'Clay');
  hcb.Label.String = 'Depth';

