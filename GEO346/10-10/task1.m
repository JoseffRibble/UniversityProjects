clear
close all
T = readtable("Lakegrainsize.xls");

terplot
[hd, hcb] = ternaryc(T.Sand_proportion_, T.Silt_proportion_, T.Clay_proportion_, T.Depth_m_,'o');
terlabel('Sand', 'Silt', 'Clay');

hcb.Label.String = 'Depth';
