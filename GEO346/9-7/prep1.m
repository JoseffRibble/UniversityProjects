matrix = [0.5,  -0.2   , 3   ; 
          pi ,   10    , 1.1 ;
          -8 ,  sqrt(2), 1   ];

% How many elements of the first column of A are positive?
a = sum(matrix(:,1)>=0)
% How many elements of the first row of A are negative? 
b = sum(matrix(1,:)<0)
% For which rows of A is at least one element negative?
mins = min(matrix);
idx = find(mins < 0)

