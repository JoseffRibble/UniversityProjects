function prep2(x)
  if x > 0
    disp("The number is positive")
  elseif x < 0
    disp("The number is negative")
  elseif x == 0
    disp("The number is zero")
  endif
