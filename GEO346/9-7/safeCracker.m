function safeCracker(i,j,k)
  % Correct code: 894
  if i == 8
    printf("The first digit is correct. \n")
  elseif
    printf("The first digit is incorrect. \n")
  endif
  
  if j == 9
    printf("The second digit is correct. \n")
  elseif
    printf("The second digit is incorrect. \n")
  endif
  
  if k == 4
    printf("The third digit is correct. \n")
  elseif
    printf("The third digit is incorrect. \n")
  endif

  
  % Testing values:
  % 0,0,0
  % 0,0,4
  % 0,9,0
  % 8,0,0
  % 8,0,4
  % 8,9,0
  % 8,9,4