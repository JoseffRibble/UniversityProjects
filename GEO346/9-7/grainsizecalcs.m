% More than 50% sand
sandcalc = sum(gs(:,1)) > 50;
printf("# of samples with more than 50%% sand: %f \n", sandcalc) 

% More than 50% silt
siltcalc = sum(gs(:,2)) > 50;
printf("# of samples with more than 50%% silt: %f \n", siltcalc) 

% More than 30% silt or more than 25% clay
siltclaycalc = sum(gs(:,2) > 30 | gs(:,3) > 25);
printf("# of samples with more than 30%% silt or more than 25%% clay: %f \n", siltclaycalc) 

% More than 40% sand and less than 40% silt and more than 40% clay
triplecount = sum(gs(:,1) > 40 & gs(:,2) < 40 & gs(:,3) > 40);
printf("# of samples with more than 40%% sand and less than 40%% silt and more than 40%% clay: %f \n", triplecount) 


% Testing infos:
% ran the script