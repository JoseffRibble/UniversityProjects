#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

uint64_t djb2(string str) {
    const char *ptr = str.c_str();
    uint64_t hash = 5381;
    int c;
    while ((c = *ptr++)) {
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    }
    return hash;
}

class Person {
public:
    string id, first, last, email, phone, city, state, postalCode, theKey;
    Person *next = nullptr;
    vector<Person> peopleList;
    Person() {
        this->first = "__EMPTY__";
        this->theKey = "__EMPTY__";
    }

    Person(string id, string first, string last, string email, string phone, string city, string state, string postalCode, string theKey, Person *next){
        this->id = id;
        this->first = first;
        this->last = last;
        this->email = email;
        this->phone = phone;
        this->city = city;
        this->state = state;
        this->postalCode = postalCode;
        this->theKey = theKey;
        this->next = next;
    }

    ostringstream print() {
        ostringstream os;
        os << "Id,FirstName,LastName,Email,Phone,City,State,PostalCode" << endl;
        for(Person &i : peopleList) {
            os << i.id + "," + i.first + "," + i.last + "," + i.email + "," + i.phone + "," + i.city + "," + i.state + "," + i.postalCode << endl;
        }
        return os;
    }

    void addBack(Person newPerson) {
        peopleList.push_back(newPerson);
    }

    int listSize() const{
        return peopleList.size();
    }

};

class HashTable {
public:
    string key;
    int tableSize;
    vector<Person> table;
    HashTable(vector<Person> newTable, string key, int tableSize){
        table = newTable;
        this->key = key;
        this->tableSize = tableSize;
    }

    void add(int &location, const Person& input) { //adds person at location
        int j = 0;
        bool end = true;
        while(end) {
            if (table.at(location).first == "__EMPTY__") { //if empty, place person in slot
                table.at(location).addBack(input);
                table.at(location).first = "__HEAD__";
                table.at(location).theKey = input.theKey;
                end = false;
            }else if(table.at(location).theKey == input.theKey){ //check if the person at slot has same key name
                table.at(location).addBack(input);
                end = false;
            }else { //else run again
                location = (djb2(input.theKey) + j + j * j) % tableSize;
                j++;
            }
        }
    }

    Person at(int location){
        return table.at(location);
    }

    ostringstream locate(string location) {
        ostringstream os;
        for (Person &i: table) {
            if (location == i.theKey) {
                os << i.print().str();
            }
        }
        return os;
    }

    ostringstream print(){
        ostringstream os;
        int itr = 0;
        for(Person &i : table){
            if(table.at(itr).first != "__EMPTY__"){ //make sure there is stuff in it
                int totalInSlot = i.listSize();
                os << to_string(itr) << ": " << i.theKey << "(" << to_string(totalInSlot) << "),\n";
            }
            itr++;
        }
        return os;
    }
};

void getVal(istream &is, string &str) {
    char ch;
    string line;

    is >> ch;
    getline(is, line);

    str = ch + line;
}

bool isValidKey(const string &key) {
    string validKeys[8] = { "Id", "FirstName", "LastName", "Email", "Phone", "City", "State", "PostalCode" };
    for (int i = 0; i < 8; i++) {
        if (key == validKeys[i]) {
            return true;
        }
    }
    return false;
}

int main(int argc, char **argv) {
    if (argc != 4) {
        cout << "Usage: ./a.out filename.txt table_size key" << endl;
        return 1;
    }

    string filename = argv[1];
    int tableSize = stoi(argv[2]);
    string key = argv[3];

    ifstream file(filename);
    if (!file) {
        cout << "Unable to open " << filename << endl;
        return 2;
    }

    if (!isValidKey(key)) {
        cout << "Invalid key: " << key << endl;
        return 3;
    }

    vector<Person> newTable (tableSize, Person()); //create table
    HashTable table = HashTable(newTable,key,tableSize);

    string id, first, last, email, phone, city, state, postalCode;
    // This is an example of how to retrieve the tokens from the input file
    // You will need to modify this to fit your needs to build the hash table
    string line, token;
    getline(file, line); // consume header line
    while (getline(file, line)) {
        istringstream iss(line);
        vector<string> inputs;
        while (getline(iss, token, '\t')) {
            inputs.push_back(token);
        }
        Person newPerson = Person(inputs.at(0), inputs.at(1), inputs.at(2), inputs.at(3), inputs.at(4), inputs.at(5), inputs.at(6), inputs.at(7), "toRename", nullptr); //create person
        inputs.clear();

        //convert Key to group. There is probably a better way to do this.
        string keyToUse;
        if(key == "Id"){
            keyToUse = newPerson.id;
            newPerson.theKey = newPerson.id;
        }else if(key == "FirstName"){
            keyToUse = newPerson.first;
            newPerson.theKey = newPerson.first;
        }else if (key == "LastName"){
            keyToUse = newPerson.last;
            newPerson.theKey = newPerson.last;
        }else if (key == "Email"){
            keyToUse = newPerson.email;
            newPerson.theKey = newPerson.email;
        }else if (key == "Phone"){
            keyToUse = newPerson.phone;
            newPerson.theKey = newPerson.phone;
        }else if (key == "City"){
            keyToUse = newPerson.city;
            newPerson.theKey = newPerson.city;
        }else if (key == "State"){
            keyToUse = newPerson.state;
            newPerson.theKey = newPerson.state;
        }else if (key == "PostalCode"){
            keyToUse = newPerson.postalCode;
            newPerson.theKey = newPerson.postalCode;
        }
        uint64_t b;
        int a;
        b = djb2(keyToUse);
        a = b % tableSize; //a is the hash location
        table.add(a, newPerson); //add newPerson at a
    }

    cout << "Commands:" << endl << "\tprint" << endl << "\tlookup <key>" << endl << "\tquit" << endl;
    string cmd, val;
    while (true) {
        cout << endl << "Enter a command:" << endl;
        cin >> cmd;
        if (cmd == "quit") {
            break;
        }
        else if (cmd == "print") {
            cout << table.print().str();
        }
        else if (cmd == "lookup") {
            getVal(cin, val);
            //cout << "looking up: " << val << endl;
            cout << table.locate(val).str();
        }
        else {
            getline(cin, val);
            cout << "Invalid command" << endl;
        }
    }

    return 0;
}
