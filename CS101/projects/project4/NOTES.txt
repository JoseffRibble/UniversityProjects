Overview

You will write a program that will read a tab delimited text file and store the data in a queryable hash table.
Tab Delimited Files

The input file is a tab delimited text file representing a table. Each row (represented by a line '\n') represents a person with each column (separated by tabs '\t') being the value of a particular field. The first row contains the column names. There will always be 8 columns, and the columns will always be in the order listed below. You can store all values from the file as strings. You can view the example .txt file in VS Code or Excel; a snippet is below. When creating your own test cases, use *nix line endings (LF, not CRLF) and be sure to have an empty newline at the end of the file.

The 8 column names, which will always be in this order, are as follows:

    Id
    FirstName
    LastName
    Email
    Phone
    City
    State
    PostalCode

Id    FirstName   LastName    Email   Phone   City    State   PostalCode
1    Blythe  Sawood  bsawood0@vinaora.com    309-770-8195    Carol Stream    Illinois    60158
2    Erv Attwoul eattwoul1@walmart.com   240-394-7643    Frederick   Maryland    21705
3    Elwood  Canwell ecanwell2@4shared.com   404-376-8669    Atlanta Georgia 30368
...
54    Esdras  Treby   etreby1h@taobao.com 209-874-0096    Stockton    California  95298
55    Taber   Snuggs  tsnuggs1i@stanford.edu  713-295-3367    Houston Texas   77206

Implementation Details

Download the starting files below under Submission Instructions. You will see a couple of example input files and the starting template for the program interface, which contains an example of how to read the values in the input file.

The program accepts 3 command-line arguments:

    filename - the name of the tab delimited input file
    table_size - the size of the hash table to be created
        If you need some good table size integers, this link has a list of prime numbers. Pick a prime that is roughly 1.5x to 2x the size of the number of unique keys.
    key - the column name used as a key to store the person in the table

Hash Table Details

    The hash table will use open addressing with quadratic probing as the collision resolution strategy.
    Hash Function: You must use the djb2 hash function provided to turn a string into an unsigned long/uint64_t.
        The uint64_t type is an unsigned 64-bit int (the int type is a signed 32-bit int). Be sure to store the return value of djb2 appropriately.
    Mapping Function: Use h(key) = djb2(key) % size to map the string key to an index in the table.
        You can store the result of djb2(key) % size as an int, as the result of uint64_t % int will be an int for this project.
        Examples where int size = 11;:
            uint64_t a = djb2(key); int b = a % size; is good
            int a = djb2(key) % size; is good
            int a = djb2(key); is bad
    Collision Resolution: Use quadratic probing when there is a collision as shown in the book: (h(key) + j + j2) % size
    The key used for inserting people into the hash table is determined by a command-line argument. It will be one of the column names.
    Because of open addressing, each index of the hash table can store only one key. Keys are not guaranteed to be unique (suppose key == "State", multiple people have the same State value), so the hash table needs to support storing multiple people with the same key per index.

Interface Operations

    print - print all indices in the hash table that are occupied using the format in the examples.
        <index>: key (#people_in_key),
    lookup <key> - print all people with the key using the format in the examples.
        The people are printed in order of appearance in the input file.
        Print "No results" if there are no people with the specified key.
    quit - stop accepting commands and close the program.

Other Requirements

    You must define and use classes. One must be named HashTable.
        You have the freedom to design the classes (their fields and methods) as you see fit.
    You can use the vector and list classes.
    Do not include libraries other than iostream, fstream, sstream, iomanip, string, cctype, cmath, vector, and list.

Notes

    Format your output exactly as the examples.
    Assume valid input and correctly formatted files.
    Input is case-sensitive and whitespace-sensitive.
    There is no limit to the number of rows in the input file.
    There is no limit to the number of people stored in a key.
    It is recommended to create your own test cases in addition to the examples provided.
        Random Data Generator
    You have two submissions for this project where the highest grade earned is the grade received. It is advised to submit to the tester project in Submit mode for grading to verify it is formatted correctly and passes the example cases in the ZyBooks *nix environment. You can submit to the tester unlimited times, and it is not graded.

Guide

    Download the starting files and understand the existing code.
    Modify the read-in to make a person.
    After a Person is built, store them in the HashTable.
        The hash table is an array.
        Each cell in the array should contain a list of people that share the same key.
    It is recommended to write the code to store first, print second, then lookup third.
    Draw pictures of how the data will be stored for reference as you write the code.
    Test often.

Create person
place person into table based on ID
if conflict, resolve unless if match, then append.
print
