#include <iostream> 
#include <fstream> 
#include <sstream> 
#include <string> 
#include <cctype> 
#include <climits> 
#include <cstring>

using namespace std;

void Merge(int numbers[], int i, int j, int k) {
   int mergedSize;
   int mergePos;
   int leftPos;
   int rightPos;
   int* mergedNumbers = nullptr;
   mergePos = 0;
   mergedSize = k - i + 1;
   leftPos = i;
   rightPos = j + 1;
   mergedNumbers = new int[mergedSize];
   while (leftPos <= j && rightPos <= k) {
      if (numbers[leftPos] < numbers[rightPos]) {
         mergedNumbers[mergePos] = numbers[leftPos];
         ++leftPos;
      }
      else {
         mergedNumbers[mergePos] = numbers[rightPos];
         ++rightPos;
         
      }
      ++mergePos;
   }
   while (leftPos <= j) {
      mergedNumbers[mergePos] = numbers[leftPos];
      ++leftPos;
      ++mergePos;
   }
   while (rightPos <= k) {
      mergedNumbers[mergePos] = numbers[rightPos];
      ++rightPos;
      ++mergePos;
   }
   for (mergePos = 0; mergePos < mergedSize; ++mergePos) {
      numbers[i + mergePos] = mergedNumbers[mergePos];
   }
   delete[] mergedNumbers;
}

void Mergesort(int numbers[], int i, int k) {
   int j;
   
   if (i < k) {
      j = (i + k) / 2;
      Mergesort(numbers, i, j);
      Mergesort(numbers, j + 1, k);
      Merge(numbers, i, j, k);
   }
}

int Partition(string strings[], int i, int k) {
   int l;
   int h;
   int midpoint;
   string pivot;
   string temp;
   bool done;
   midpoint = i + (k - i) / 2;
   pivot = strings[midpoint];
   done = false;
   l = i;
   h = k;
   while (!done) {
      while (strings[l] < pivot) {
         ++l;
      }
      while (pivot < strings[h]) {
         --h;
      }
      if (l >= h) {
         done = true;
      }
      else {
         temp = strings[l];
         strings[l] = strings[h];
         strings[h] = temp;
         
         ++l;
         --h;
      }
   }
   
   return h;
}

void Quicksort(string strings[], int i, int k) {
   int j;
   if (i >= k) {
      return;
   }
   j = Partition(strings, i, k);
   Quicksort(strings, i, j);
   Quicksort(strings, j + 1, k);
}

int main(int argc, char* argv[]){
    ifstream file1;
    ifstream file2;
    string type = argv[1];
    //file1 = argv[2];
    //file2 = argv[3];
    file1.open(argv[2]);
    file2.open(argv[3]);

    //filelengths of the 2 files... needed????
    string line = "";
    int fileLength1temp = 0;
    while(getline(file1, line)){ 
        ++fileLength1temp;
    }
    file1.close();
    file1.open(argv[2]);

    int fileLength2temp = 0;
    while(getline(file2, line)){ 
        ++fileLength2temp;
    }
    file2.close();
    file2.open(argv[3]);

    if(type == "i"){
        const int fileLength1 = fileLength1temp;
        const int fileLength2 = fileLength2temp;
        int list1[fileLength1];
        int list2[fileLength2];
        for(int i = 0; i < fileLength1; ++i){
            file1 >> list1[i];
        }
        for(int i = 0; i < fileLength2; ++i){
            file2 >> list2[i];
        }

        int sames[fileLength1];
        fill(sames, sames+fileLength1, INT_MIN); //this is a very jank solution that will fail if any value is -2147483648
        int out[fileLength1];
        fill(out, out+fileLength1, INT_MIN); 

        Mergesort(list1, 0, fileLength1 - 1);
        Mergesort(list2, 0, fileLength2 - 1);

        for(int i = 0, k = 0; i < fileLength1 && k < fileLength2;){
            if(list1[i] == list2[k]){
                sames[i] = list1[i];
                ++i;
                ++k;
            }
            else if(list1[i] < list2[k]){
                ++i;
            }
            else{
                ++k;
            }
        }

        int nextout = 0;
        for(int i = 0; i < fileLength1; ++i){
            if(sames[i] != INT_MIN){
               out[nextout] = sames[i];
               ++nextout;
            }
        } 

        //remove duplicates
        int next = 0;
        int temp[fileLength1];
        fill(temp, temp+fileLength1, INT_MIN);
        for(int i = 0; i < fileLength1 - 1; ++i){
            if(out[i] != out[i + 1]){
                temp[next] = out[i];
                next++;
            }
        }
        temp[next] = out[fileLength1 - 1]; 

        for(int i = 0; i < fileLength1 - 1; ++i){
            if(temp[i] != INT_MIN){
                cout << temp[i] << endl;
            }
        }

        //break out?
        type = "q";


    }
    else if(type == "s"){
        const int fileLength1 = fileLength1temp;
        const int fileLength2 = fileLength2temp;
        string list1[fileLength1];
        string list2[fileLength2];
        for(int i = 0; i < fileLength1; ++i){
            file1 >> list1[i];
        }
        for(int i = 0; i < fileLength2; ++i){
            file2 >> list2[i];
        }
        string sames[fileLength1];

        Quicksort(list1, 0, fileLength1 - 1);
        Quicksort(list2, 0, fileLength2 - 1);

        for(int i = 0, k = 0; i < fileLength1 && k < fileLength2;){
            if(list1[i] == list2[k]){
                sames[i] = list1[i];
                ++i;
                ++k;
            }
            else if(list1[i] < list2[k]){
                ++i;
            }
            else{
                ++k;
            }
        }

        string out[fileLength1];
        int nextout = 0;
        for(int i = 0; i < fileLength1; ++i){
            if(sames[i] != "\0"){
               out[nextout] = sames[i];
               ++nextout;
            }
        } 

        int next = 0;
        string temp[fileLength1];
        for(int i = 0; i < fileLength1 - 1; ++i){
            if(out[i] != out[i + 1]){
                temp[next] = out[i];
                next++;
            }
        }
        temp[next] = out[fileLength1 - 1]; 

        for(int i = 0; i < fileLength1 - 1; ++i){
            if(temp[i] != "\0"){
                cout << temp[i] << endl;
            }
        }
        type = "q";
        
    }

    else{
        return 0;
    }

}