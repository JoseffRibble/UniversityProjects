#include <iostream>
#include <cmath>

using namespace std;

class Term {
public:
    double coef;
    int degree;
    Term *next;

    Term(double coef = 0, int degree = 0, Term *next = nullptr) {
        this->coef = coef;
        this->degree = degree;
        this->next = next;
    }

    void print(ostream &os) {
        if (degree == 0) {
            os << coef;
        }
        else if (degree == 1) {
            os << coef << "x";
        }
        else {
            os << coef << "x^" << degree;
        }
    }

    int compare(Term *rhs) {
        Term *lhs = this;
        if (lhs->degree < rhs->degree) return -1;
        if (lhs->degree > rhs->degree) return 1;
        if (lhs->coef < rhs->coef) return -1;
        if (lhs->coef > rhs->coef) return 1;
        return 0;
    }
};

class Poly {
private:
    Term *head;
public:
    Poly() {
        head = nullptr;
    }

    void addFront(double coef, int degree) {
        Term *n = new Term(coef, degree, head);
        head = n;
    }

    void addBack(double coef, int degree) {
        if (head == nullptr) { // empty
            addFront(coef, degree);
        }
        else { // nonempty
            Term *cur = head;
            while (cur->next != nullptr) {
                cur = cur->next;
            }
            cur->next = new Term(coef, degree);
        }
    }

    void addOrdered(double coef, int degree) {
        /*
            1. empty list
                - addFront()
            2. nonempty list
                2.1 addFront() when new->degree > head->degree
                2.2 addMiddle() & addBack()
                    - traverse with prev & cur
                    - prev->next = new
                    - new->next = cur
        */
       Term *n = new Term(coef, degree);
       if (head == nullptr)  { // empty list - addFront
           head = n;
       }
       else if (n->compare(head) > 0) { // nonempty list - addFront
            n->next = head;
            head = n;
       }
       else { // nonempty list - addMiddle & addBack
            Term *prev = head;
            Term *cur = head->next;
            while (cur != nullptr) {
                if (n->compare(cur) > 0) {
                    break;
                }
                prev = prev->next;
                cur = cur->next;
            }
            prev->next = n;
            n->next = cur;
       }
    }

    void print(ostream &os) {
        for (Term *cur = head; cur != nullptr; cur = cur->next) {
            if (cur != head) {
                os << " + ";
            }
            cur->print(os);
            // if (cur->next != nullptr)
            //     os << " + ";
        }
        os << endl;
    }

    Term *search(int degree) {
        for (Term *cur = head; cur != nullptr; cur = cur->next) {
            if (degree == cur->degree) {
                return cur;
            }
        }
        return nullptr;
    }

    double eval(int x) {
        double total = 0;
        for (Term *cur = head; cur != nullptr; cur = cur->next) {
            total += cur->coef * pow(x, cur->degree);
        }
        return total;
    }

    bool remove(int degree) {
        // verify exists
        // delete front
        //  - head = head->next
        //  - delete old head
        // delete middle & back
        //  - traverse
        //  - prev->next = cur->next
        //  - delete cur
        if (!search(degree)) {
            return false;
        }
        else if (head->degree == degree) {
            Term *oldHead = head;
            head = head->next;
            delete oldHead;
        }
        else {
            Term *prev = head;
            Term *cur = head->next;
            while (cur != nullptr) {
                if (cur->degree == degree) {
                    break;
                }
                prev = prev->next;
                cur = cur->next;
            }
            prev->next = cur->next;
            delete cur;
        }
        return true;
    }

    void clear() {
        while (head != nullptr) {
            remove(head->degree);
        }
    }
    
    void deepCopy(Poly &src) {
        for (Term *cur = src.head; cur != nullptr; cur = cur->next) {
            addBack(cur->coef, cur->degree);
        }
    }

    Poly(Poly &src) { // copy constructor
        head = nullptr;
        deepCopy(src);
    }

    Poly &operator=(Poly &src) { // assignment operator
        if (this != &src) {
            clear();
            deepCopy(src);
        }
        return *this;
    }

    ~Poly() { // destructor
        clear();
    }
};

int main() {
    Poly poly;
    poly.addOrdered(1,6); // empty list - addFront
    poly.addOrdered(1,4); // addBack no traverse
    poly.addOrdered(1,3); // addBack with traverse
    poly.addOrdered(1,1); // addBack with traverse
    poly.addOrdered(1,8); // addFront
    poly.addOrdered(1,5); // addMiddle with traverse
    poly.addOrdered(1,2); // addMiddle with traverse
    poly.addOrdered(1,7); // addMiddle no traverse
    poly.print(cout);

    // poly.clear();
    // poly.print(cout);
    // poly.addOrdered(3,3);
    // poly.addOrdered(2,2);
    // poly.print(cout);

    Poly poly2(poly); // poly2 = poly;
    poly2.print(cout);
    cout << "--" << endl;

    poly2.remove(1);

    poly.print(cout);
    poly2.print(cout);

    poly = poly;


    // poly.remove(8); // front
    // poly.print(cout);
    // poly.remove(1); // back
    // poly.print(cout);
    // poly.remove(3); // middle
    // poly.print(cout);
    // poly.remove(6); // second
    // poly.print(cout);
    // poly.remove(4);
    // poly.remove(2);
    // poly.remove(5);
    // poly.remove(7);
    // poly.print(cout); // empty list

    // poly.addOrdered(1,1);
    // poly.addOrdered(2,2);
    // poly.addOrdered(3,3);
    // poly.print(cout);

    return 0;
}
