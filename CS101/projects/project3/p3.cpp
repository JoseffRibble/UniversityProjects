#include <iostream>
#include <string>

#include "p3.h"

using namespace std;

Info::Info() {
    this->name = "No Name Set";
    this->value = "No Value Set";
    this->next = nullptr;
}

Info::Info(std::string name, std::string value, Info *next) {
    this->name = name;
    this->value = value;
    this->next = next;
}

Contact::Contact() {
    this->first = "No First Set";
    this->last = "No Last Set";
    this->next = nullptr;
    this->headInfoList = nullptr;
}

Contact::Contact(std::string first, std::string last, Contact *next) {
    this->first = first;
    this->last = last;
    this->next = next;
    this->headInfoList = nullptr;
}

int Contact::Compare(Contact *rhs){
    Contact *lhs = this;
    if (lhs->last < rhs->last) return 1;
    if (lhs->last > rhs->last) return -1;
    if (lhs->first < rhs->first) return 1;
    if (lhs->first > rhs->first) return -1;
    return 0;
}

int Info::CompareInfo(Info *rhs){
    Info *lhs = this;
    if (lhs->name < rhs->name) return 1;
    if (lhs->name > rhs->name) return -1;
    return 0;
}

Contact *ContactList::searchContacts(std::string first, std::string last) { //searches contacts. returns contact if found, else returns nullptr
    for (Contact *cur = headContactList; cur != nullptr; cur = cur->next) {
        if (first == cur->first && last == cur->last) {
            return cur;
        }
    }
    return nullptr;
}

ContactList::ContactList() {
    this->headContactList = nullptr;
    this->count = 0;
}

int ContactList::getCount() {
    return this->count;
}

//DONE
// print the specified contact and its information
// 1. return false and print nothing if the contact is not in the list
// 2. otherwise return true and print the contact
bool ContactList::printContact(std::ostream &os, std::string first, std::string last) {
    for (Contact *cur = headContactList; cur != nullptr; cur = cur->next) {
        if (first == cur->first && last == cur->last) {
            if(cur->headInfoList != nullptr){ //check if there is more than a name
                os << "Contact Name: " << cur->first << " " << cur->last << endl;
                for(Info *info = cur->headInfoList; info != nullptr;){ //move through info
                    os << "    " << info->name << " | " << info->value << endl;
                    info = info->next;
                }
            }
            else {
                os << "Contact Name: " << first << " " << last << endl;
            }
            return true;
        }
    }
    return false; //changed to true
}

//DONE
// print all contacts and their information
// print nothing if the list is empty
void ContactList::print(std::ostream &os) {
    for (Contact *cur = headContactList; cur != nullptr; cur = cur->next) {
        if(cur->headInfoList != nullptr){ //check if there is more than a name
            os << "Contact Name: " << cur->first << " " << cur->last << endl;
                for(Info *info = cur->headInfoList; info != nullptr;){ //move through info
                    os << "    " << info->name << " | " << info->value << endl;
                    info = info->next;
                }
        }
        else { //only print name
            os << "Contact Name: " << cur->first << " " << cur->last << endl;
        }
    }
}

//DONE
// add a contact to the back of the list
// 1. return false and do nothing if the contact is already in the list
// 2. otherwise return true and add the contact to the back of the list
// - do not forget to update count
bool ContactList::addContact(std::string first, std::string last) {
    if(headContactList == nullptr){ //check if empty
        Contact *n = new Contact(first, last, nullptr);
        headContactList = n;
        count++;
    }
    else {
        if(searchContacts(first, last)){
            return false;
        }
        Contact *curr = headContactList;
        while (curr->next != nullptr) {
            curr = curr->next;
        }
        curr->next = new Contact(first, last, nullptr);
        count++;
    }
    return true;
}

// add info to the back of a contact's info list
// 1. return false and do nothing if the contact is not in the list
// 2. if the infoName is already in the info list, update the infoValue and return true
// 3. otherwise add the info to the back of the contact's list and return true
bool ContactList::addInfo(std::string first, std::string last, std::string infoName, std::string infoVal) {
    for(Contact *cur = headContactList; cur != nullptr; cur = cur->next) { //go through contact list
        if(first == cur->first && last == cur->last){ //do if name found
            if (cur->headInfoList == nullptr) { //if there is already no information, add to front
                Info *n = new Info(infoName, infoVal, nullptr);
                cur->headInfoList = n;
            } else { //if there is already information, search for matching name and update or add to back
                Info *curr = cur->headInfoList;
                while(curr != nullptr){
                    if(infoName == curr->name){ //if value exists, update info
                        curr->value = infoVal;
                        return true;
                    }
                    curr = curr->next;
                }
                //Doesn't exist, add info to back
                Info *n = cur->headInfoList;
                while (n->next != nullptr) {
                    n = n->next;
                }
                n->next = new Info(infoName, infoVal);
            }
            return true;
        }
    }
    return false;
}

// add a contact to the list in ascending order by last name
//     if last names are equal, then order by first name ascending
// 1. return false and do nothing if the contact is already in the list
// 2. otherwise return true and add the contact to the list
// - do not forget to update count
// - compare strings with the built-in comparison operators: <, >, ==, etc.
// - a compare method/function is recommended
bool ContactList::addContactOrdered(std::string first, std::string last) {
    for(Contact *cur = headContactList; cur != nullptr; cur = cur->next) { //go through contact list
        if(first == cur->first && last == cur->last) { //if name found
            return false;
        }
    }
    Contact *n = new Contact(first, last);
    if (headContactList == nullptr)  { //empty list
        headContactList = n;
    } else if (n->Compare(headContactList) > 0) { // nonempty list - add to front
        n->next = headContactList;
        headContactList = n;
    }
    else { // nonempty list
        Contact *prev = headContactList;
        Contact *cur = headContactList->next;
        while (cur != nullptr) {
            if (n->Compare(cur) > 0) {
                break;
            }
            prev = prev->next;
            cur = cur->next;
        }
        prev->next = n;
        n->next = cur;
    }
    count++;
    return true;
}

// add info to a contact's info list in ascending order by infoName
// 1. return false and do nothing if the contact is not in the list
// 2. if the infoName is already in the info list, update the infoValue and return true
// 3. otherwise add the info to the contact's list and return true
bool ContactList::addInfoOrdered(std::string first, std::string last, std::string infoName, std::string infoVal) {
    for(Contact *cur = headContactList; cur != nullptr; cur = cur->next) { //go through contact list
        if(first == cur->first && last == cur->last) { //if name found
            Info *n = new Info(infoName, infoVal, nullptr);
            if (cur->headInfoList == nullptr) { //the list is empty, add to front
                cur->headInfoList = n;
            }
            else if (n->CompareInfo(cur->headInfoList) > 0) { //the first spot is correct
                if(infoName == cur->headInfoList->name){ //if value exists, update info
                    n->value = infoVal;
                    return true;
                }
                n->next = cur->headInfoList;
                cur->headInfoList = n;
            }
            else { //search through list to find a spot
                Info *prev = cur->headInfoList;
                Info *curr = cur->headInfoList->next;
                while (curr != nullptr) {
                    if (n->CompareInfo(curr) > 0) {
                        break;
                    }
                    prev = prev->next;
                    curr = curr->next;
                }
                if(infoName == prev->name){ //if value exists, update info
                    prev->value = infoVal;
                    return true;
                }
                prev->next = n;
                n->next = curr;
            }
            return true;
        }
    }
    return false;
}

// DONE
// remove the contact and its info from the list
// 1. return false and do nothing if the contact is not in the list
// 2. otherwise return true and remove the contact and its info
// - do not forget to update count
bool ContactList::removeContact(std::string first, std::string last) {
    if (!searchContacts(first, last)) { //does it exist?
        return false;
    }
    else if (headContactList->first == first && headContactList->last == last) { //if we are removing the head, point headContactList to new head
        Contact *toDelete = headContactList;
        headContactList = headContactList->next;
        delete toDelete;
        count--;
    }
    else {
        Contact *prev = headContactList;
        Contact *cur = headContactList->next;
        while (cur != nullptr) {
            if (cur->first == first && cur->last == last) { //move prev and cur through list looking for match
                break;
            }
            prev = prev->next;
            cur = cur->next;
        }
        prev->next = cur->next; //point the previous pointer to the next pointer after deleted contact
        delete cur;
        count--;
    }
    return true;
}

//DONE
// remove the info from a contact's info list
// 1. return false and do nothing if the contact is not in the list
// 2. return false and do nothing if the info is not in the contact's info list
// 3. otherwise return true and remove the info from the contact's list
bool ContactList::removeInfo(std::string first, std::string last, std::string infoName) {
    for(Contact *cur = headContactList; cur != nullptr; cur = cur->next) { //go through contact list
        if(first == cur->first && last == cur->last){ //do if name found
            for(Info *curr = cur->headInfoList; curr != nullptr; curr = curr->next) { //search through info
                if(infoName == curr->name) { //if value exists, remove it
                    if (cur->headInfoList->name == infoName) { //if we are removing the head, point headInfoList to new head
                        Info *toDelete = cur->headInfoList;
                        cur->headInfoList = cur->headInfoList->next;
                        delete toDelete;
                    }
                    else {
                        Info *prev = cur->headInfoList;
                        Info *current = cur->headInfoList->next;
                        while (current != nullptr) {
                            if (current->name == infoName) { //move prev and cur through list looking for match
                                break;
                            }
                            prev = prev->next;
                            current = current->next;
                        }
                        prev->next = current->next; //point the previous pointer to the next pointer after deleted contact
                        delete current;
                    }
                    return true;
                }
            }
        }
    }
    return false;
}

//who knows
// destroy the list by removing all contacts and their infos
ContactList::~ContactList() {
    while(headContactList != nullptr){ //clear
        removeContact(headContactList->first, headContactList->last);
    }
    count = 0;
}

//seems to work
// deep copy the source list
// - do not forget to update count
ContactList::ContactList(const ContactList &src) {
    for (Contact *cur = src.headContactList; cur != nullptr; cur = cur->next) { //deepcopy
        addContact(cur->first, cur->last);
        count++;
        for(Info *curr = cur->headInfoList; curr != nullptr; curr = curr->next){
            addInfo(cur->first, cur->last, curr->name, curr->value);
        }
    }
}

//seems to work
// remove all contacts and their info then deep copy the source list
// - do not forget to update count
const ContactList &ContactList::operator=(const ContactList &src) {
    if (this != &src) {
        while(headContactList != nullptr){ //clear
            removeContact(headContactList->first, headContactList->last);
        }
        for (Contact *cur = src.headContactList; cur != nullptr; cur = cur->next) { //deepcopy
            addContact(cur->first, cur->last);
            count++;
            for(Info *curr = cur->headInfoList; curr != nullptr; curr = curr->next){
                addInfo(cur->first, cur->last, curr->name, curr->value);
            }
        }
    }
    return *this;
}
