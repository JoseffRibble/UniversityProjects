#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>

using namespace std;

class node{
public:
    int value;
    node *left, *right;

    explicit node(int in){
        value = in;
        left = nullptr;
        right = nullptr;
    }
};

vector<int> subvector(vector<int> const &v, int start, int end){
    auto first = v.begin() + start;
    auto last = v.begin() + end + 1;
    vector<int> vector(first, last);
    if(start > end){
        return vector;
    }
    while(vector.back() == 0){
        vector.pop_back();
    }
    return vector;
}

vector<int> copyMatching(const vector<int>& toCopy, const vector<int>& toChange){
    vector<int> temp;
    for(int i : toChange){
        for(int j : toCopy){
            if(i == j){
                temp.push_back(j);
            }
        }
    }
    return temp;
}

node* createTree(vector<int> inorder, vector<int> levelorder){
    if(inorder.empty() || levelorder.empty()){ //reached end
        return nullptr;
    }

    int index = -1;
    for (int i = 0; i < inorder.size(); i++){ //from the beginning to end, find index of inorder the levelorder root for this selection
        if (levelorder.front() == inorder.at(i)) {
            index = i;
        }
    }
    if(index == -1){
        return nullptr;
    }

    node* newNode = new node(inorder.at(index));
    if(inorder.size() == 1){
        return newNode;
    }

    //create subvectors
    vector<int> IOFirstHalf = subvector(inorder, 0, index - 1);
    vector<int> IOSecondHalf = subvector(inorder, index + 1, inorder.size() - 1);
    vector<int> LOFirstHalf = copyMatching(IOFirstHalf, levelorder);
    vector<int> LOSecondHalf = copyMatching(IOSecondHalf, levelorder);
    newNode->left = createTree(IOFirstHalf, LOFirstHalf);
    newNode->right = createTree(IOSecondHalf, LOSecondHalf);

    return newNode;
}

int main(int argc, char *argv[]) {
    vector<int> inorder, levelorder;
    string encoded;

    //read files
    int var;
    string temp;
    ifstream file1(argv[1]);
    while (file1 >> var) {
        inorder.push_back(var);
    }
    ifstream file2(argv[2]);
    while (file2 >> var) {
        levelorder.push_back(var);
    }
    ostringstream encodedOS;
    ifstream file3(argv[3]);
    while(file3 >> encoded) {
        encodedOS << encoded;
    }
    node *root = createTree(inorder, levelorder);

    encodedOS << 'q';
    encoded = encodedOS.str();
    node* cur = root;
    while(!encoded.empty()){
        char val = encoded.at(0);
        if(val == 'q'){
            cout << (char)cur->value;
            encoded = encoded.substr(1);
        }
        else if((val == '0' && cur->left == nullptr) || (val == '1' && cur->right == nullptr)){
            cout << (char)cur->value;
            cur = root;
        }
        else if(val == '0'){
            cur = cur->left;
            encoded = encoded.substr(1);
        } else if(val == '1'){
            cur = cur->right;
            encoded = encoded.substr(1);
        }
    }
    delete cur;
    return 0;
}