#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cctype>
using namespace std;

int calculateSrc(string isrc[], int ivalues[], string name, int length){
    int srcTotal = 0;
    for(int i = 0; i < length; ++i){
        if(isrc[i] == name){
            srcTotal += ivalues[i];
        }
    }
    int temp = srcTotal;
    srcTotal = 0;
    return temp;
}

int calculateDest(string idest[], int ivalues[], string name, int length){
    int destTotal = 0;
    for(int i = 0; i < length; ++i){
        if(idest[i] == name){
            destTotal += ivalues[i];
        }
    }
    int temp = destTotal;
    destTotal = 0;
    return temp;
}

int main(int argc, char* argv[]){
    ifstream inFS;
    ofstream outFS;
    
    inFS.open(argv[1]);
    outFS.open(argv[2]);

    string line;
    int fileLength = 0;

    while(getline(inFS, line)){ //get file length because I can't use vectors
        ++fileLength;
    }
    inFS.close();
    inFS.open(argv[1]); //make it run through again      

    //process fileIn by creating a matrix and placing values into it. IDK why I did it this way I just felt like it.
    string matrix[fileLength][3]; //create a matrix
    string entry, temp;
    for(int r = 0; r < fileLength; r++){ //rows
        for(int c = 0; c < 3; c++){ //columns
            inFS >> temp;
            for (int i = 0; temp[i] != '\0'; ++i) {
                temp[i] = toupper(temp[i]);
            }
            matrix[r][c] = temp;
        }
    }
    inFS.close();


    string output1[fileLength * 2]; //init output array
    for (int r = 0; r < 2; ++r) {
        for (int c = 0; c < fileLength; ++c) {
            output1[c * 2 + r] = matrix[c][r]; //place matrix into array
        }
    }
    int length = sizeof(output1) / sizeof(string);
    int L = length;
    
    //remove duplicates
    for(int i = 0 ;i < L; i++){
        for(int j = i + 1; j < L; j++){
            if(output1[i] == output1[j]){
                for(int k = j; k < L; k++){
                    output1[k] = output1[k+1];
                }
                j--;
                L--;
            }
        }
    }

    for (int i = 0; i < L; ++i){
        outFS << output1[i] << endl; //print part 1
    }
    outFS << endl;

    //src array
    string src[fileLength];
    for (int r = 0; r < 1; ++r) {
        for (int c = 0; c < fileLength; ++c) {
            src[c] = matrix[c][r]; 
        }
    }
    //dest array
    string dest[fileLength];
    for (int r = 1; r < 2; ++r) {
        for (int c = 0; c < fileLength; ++c) {
            dest[c] = matrix[c][r]; 
        }
    }
    //values array
    int values[fileLength];
    for (int r = 2; r < 3; ++r) {
        for (int c = 0; c < fileLength; ++c) {
            values[c] = stoi(matrix[c][r]); 
        }
    }

    //inital nonleaf list
    string initNL[fileLength * fileLength];
    //check for nonleafs
    int nextSlot = 0;
    for(int i = 0; i < fileLength; ++i){
        for(int j = 0; j < fileLength; ++j){
            if(src[i] == dest[j]){
                initNL[nextSlot] = src[i];
                ++nextSlot;
            }
        }
    }
    //clean initNL
    int L1 = fileLength * fileLength;
    for(int i = 0; i < L1; ++i){ //run through left
        for(int j = i + 1; j < L1; ++j){  //run through right
            if(initNL[i] == initNL[j]){ //check if same
                for(int k = j; k < L1 - 1; k++){ //shift remaining
                    initNL[k] = initNL[k + 1];
                }
            j--;
            L1--;
            }
        }
    }
    //nonleaf total
    int NLtotal = 0;
    for(int i = 0; initNL[i] != "\0"; ++i){
        NLtotal++;
    }
    string NL[NLtotal]; //remake smaller list
    for(int i = 0; initNL[i] != "\0"; ++i){
        NL[i] = initNL[i];
    }
    //reorder NL
    int next = 0;
    string newNL[NLtotal];
    for(int i = 0; i < L; ++i){
        for(int j = 0; j < NLtotal; ++j){
            if(output1[i] == NL[j]){
                newNL[next] = output1[i];
                next++;
            }
        }
    }

    //final output and math
    for(int i = 0; i < NLtotal; ++i){
        int value1 = calculateSrc(src, values, newNL[i], fileLength);
        int value2 = calculateDest(dest, values, newNL[i], fileLength);
        outFS << newNL[i] << " " << value1 << " " << value2 << " " << fixed << setprecision(2) << (double)value2/value1 << endl;
    }
    outFS.close();
    return 0;
}
